<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        @foreach($sliders as $k=>$slider)
        <div class="item {{ ($k==0)? 'active':'' }}">
            <img class="{{ ($k==0)? 'first':($k==1)? 'second':'third' }}-slide" src="uploads/{{ $slider->image }}" alt="{{ ($k==0)? 'first':($k==1)? 'second':'third' }} slide">
            <div class="container">
                <div class="carousel-caption">
              
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>