<html>
	<head>
	<title></title>
	</head>
<body>
	<p>register</p>
	@if(count($errors) > 0)
  	<div class="alert alert-danger">
    @foreach($errors->all() as $error)
      <p>{{ $error }}</p>
      @endforeach
    </div>
    @endif
	<form method="post" action="/register">

		<input type="text" placeholder="email" name="email" value="{{ old('email') }}">
		<input type="text" placeholder="full name" name="name" value="{{ old('name') }}">
		<input type="text" placeholder="phone" name="phone" value="{{ old('phone') }}">
		<input type="password" placeholder="password" name="password">
		<input type="password" placeholder="confirm password" name="cnfpassword">
		{{ csrf_field() }}
		<input type="submit">
	</form>
</body>	
</html>