
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<link href="css/stylelogin.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text.css'/>
		<!--//webfonts-->
</head>
<body>
	<div class="main">
		<div class="header" >
			<h1>Login or Create a Free Account!</h1>
		</div>
		<p>Welcome to Naval Services. </p>
		@if(count($errors) > 0)
				  	<div class="alert alert-danger">
				    @foreach($errors->all() as $error)
				      <p>{{ $error }}</p>
				      @endforeach
				    </div>
				    @endif
			<form method="post" action="/register"> 
				<ul class="left-form">
					<h2>New Account:</h2>
					<li>
						<input type="text"   placeholder="Email" name="email"  value="{{ old('email') }}" required/>
						<a href="#" class="icon ticker"> </a>
						<div class="clear"> </div>
					</li> 
					<li>
						<input type="text" placeholder="Full Name" name="name" value="{{ old('name') }}" required/>
						<a href="#" class="icon ticker"> </a>
						<div class="clear"> </div>
					</li> 
					<li>
						<input type="text" placeholder="Phone Number" name="phone" value="{{ old('phone') }}" required/>
						<a href="#" class="icon ticker"> </a>
						<div class="clear"> </div>
					</li> 
					<li>
						<input type="password" placeholder="password" name="password" required/>
						<a href="#" class="icon ticker"> </a>
						<div class="clear"> </div>
					</li> 
					<li>
						<input type="password" placeholder="confirm password" name="cnfpassword" required/>
						<a href="#" class="icon ticker"> </a>
						<div class="clear"> </div>
					</li> 
					{{ csrf_field() }}
<!-- 					<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Please inform me of upcoming  NAVAL Services, Promotions and news</label> -->
					<input type="submit" value="Create Account">
						<div class="clear"> </div>
				</ul>
			</form>
			<form method="post" action="/login">	
				<ul class="right-form">
					<h3>Login:</h3>
					<div>
						<li><input type="text" name="email" value="{{ old('email') }}" placeholder="Email" required/></li>
						<li> <input type="password" name="password" placeholder="Password" required/></li>
						<input type="submit" value="Login" >
					</div>{{ csrf_field() }}

					<div class="clear"> </div>
				</ul>
				<div class="clear"> </div>
			</form>
			
		</div>
			<!-----start-copyright---->
   					
				<!-----//end-copyright---->

	
</body>
</html>