<section class="our-gallery" id="clients">
    <h3 class="text-center slideanim">Our Clients</h3>
    <p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 
    <div class="container">
        <div class="content slideanim">
            <div class="chroma-gallery mygallery">
                @foreach($clients as $client)
                <img src="uploads/{{ $client->client_image }}" alt="{{ $client->client_name }}" data-largesrc="{{ $client->client_image }}">
                @endforeach
            </div>
        </div>
    </div>
</section>