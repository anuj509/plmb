<!DOCTYPE html>
<html>
<head>
    <title>Naval Admin </title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="base_url" content="{{url('')}}" id="base_url"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/select2/css/select2.min.css')}}">
    @if(Session::get('lang')==='ar')
        <link rel="stylesheet" href="{{url('assets/css/yep-rtl.min.css')}}">
    @endif
    {{--multi ajax uploader--}}
    <link rel="stylesheet" href="{{url('assets/vendors/dropzone/css/basic.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/dropzone/css/dropzone.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/ui-select/css/select.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/angular-wizard/css/angular-wizard.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/angular-ui-notification/css/angular-ui-notification.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/sweetalert/css/sweetalert.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap-duallistbox/css/bootstrap-duallistbox.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap-daterangepicker/css/daterangepicker.min.css')}}">
    <!-- Related css to this page --><!-- don't remove this comment -->
    {{--<link rel="stylesheet" href="{{url('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}">--}}

    <!-- Yeptemplate css --><!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{url('assets/css/yep-style.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/yep-custom.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/yep-vendors.css')}}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{url('assets/img/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{url('assets/img/favicon/favicon.ico')}}" type="image/x-icon">
    <script type="text/javascript" src="{{url('assets/vendors/jquery/jquery.min.js')}}"></script>
    <style>
        .main {
            min-height: 725px;
        }
    </style>
</head>

<body id="mainbody"  @if(Session::get('lang')==='ar')class="rtl" @endif lang="{{Session::get('lang')}}">
<div id="container" class="container-fluid skin-3 ">

    <header id="header">
        <nav class="navbar navbar-default nopadding" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <button type="button" id="menu-open" class="navbar-toggle menu-toggler pull-left">
                    <span class="sr-only">Toggle sidebar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" id="logo-panel">
                    <img src="#" alt="Naval">
                </a>
            </div>
            <form action="#" class="form-search-mobile pull-right">
                <input id="search-fld" class="search-mobile" type="text" name="param" placeholder="Search ...">
                <button id="submit-search-mobile" type="submit">
                    <i class="fa fa-search"></i>
                </button>
                <a href="#" id="cancel-search-mobile" title="Cancel Search"><i class="fa fa-times"></i></a>
            </form>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li id="search-show-li" class="dropdown">
                        <a href="#" id="search-mobile-show" class="dropdown-toggle" >
                            <i class="fa fa-search"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language fa-lg primary"></i></span></a>
                        <ul class="dropdown-menu">
                            <li class="@if(Session::get('lang')=='en') active @endif"><a href="{{url("/admin/language?lang=en")}}">English </a></li>
                            <li class="@if(Session::get('lang')=='ar') active @endif"><a href="{{url("/admin/language?lang=ar")}}">Arabic </a></li>
                            <li class="@if(Session::get('lang')=='pt') active @endif"><a href="{{url("/admin/language?lang=pt")}}">Brazil </a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img alt="{{isset(Auth::user()->name)?Auth::user()->name:''}}" src="{{ \Session::get('avatar_url') }}" height="50" width="50" class="img-circle" />
                            {{isset(Auth::user()->name)?Auth::user()->name:''}}
                            <strong class="caret"></strong>
							 
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{url("admin/users")}}">{{ trans('main.profile') }}<span class="fa fa-user pull-right"></span></a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="{{url("/auth/logout")}}">{{ trans('main.sign_out') }}<span class="fa fa-power-off pull-right"></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li id="fullscreen-li">
                        <a href="#" id="fullscreen" class="dropdown-toggle" >
                            <i class="fa fa-arrows-alt"></i>
                        </a>
                    </li>

                    <li id="side-hide-li" class="dropdown">
                        <a href="#" id="side-hide" class="dropdown-toggle" >
                            <i class="fa fa-reorder"></i>
                        </a>
                    </li>
                </ul>
                <!-- search form in header -->
                <form class="navbar-form navbar-right" >
                    <div class="form-group">
                        <input type="text" class="form-control search-header" placeholder="{{ trans('main.enter_keyword') }}" />
                        <button type="submit" class="btn btn-link search-header-btn" >
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>

        </nav>
    </header>

    <!-- sidebar menu -->
    <div id="sidebar" class="sidebar" >
        <div class="tabbable-panel">
            <div class="tabbable-line">
                <ul class="nav nav-tabs nav-justified">
                    <li id="tab_menu_a" class="active">
                        <a href="#tab_menu_1" data-toggle="tab">
                            <i class="fa fa-reorder"></i>
                        </a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_menu_1">
                        <form class="search-menu-form" >
                            <div class="">
                                <input id="menu-list-search" placeholder="{{ trans('main.search_menu') }}" type="text" class="form-control search-menu">
                            </div>
                        </form>

                        <!-- sidebar Menu -->
                        <div id="MainMenu" class="">

                            <ul id="menu-list" class="nav nav-list">

                                <li class="separate-menu"><span>{{ trans('main.configurations') }}</span></li>

                                <li class="@if(Request::segment(2)=='users' || Request::segment(2)=='permissions' || Request::segment(2)=='roles') active open @endif">
                                    <a href="{{url('/admin/users')}}" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-user"></i>
                                        <span class="menu-text"> {{ trans('main.users_management') }}</span>
                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>
                                    <b class="arrow"></b>
                                    <ul class="submenu">
                                        @permission('view_permission')
                                        <li class="@if(Request::segment(2)=='permissions') active @endif">
                                            <a href="{{url('/admin/permissions')}}" >
                                                <i class="menu-icon fa fa-list"></i>
                                                <span class="menu-text"> {{ trans('main.permissions') }} </span>
                                            </a>
                                        </li>
                                        @endpermission
                                        @permission('view_role')
                                        <li class="@if(Request::segment(2)=='roles') active @endif">
                                            <a href="{{url('/admin/roles')}}">
                                                <i class="menu-icon fa fa-bar-chart-o"></i>
                                                <span class="menu-text"> {{ trans('main.roles') }} </span>
                                            </a>
                                        </li>
                                        @endpermission
                                        @permission('view_user')
                                        <li class="@if(Request::segment(2)=='users') active @endif">
                                            <a href="{{url('/admin/users')}}">
                                                <i class="menu-icon fa fa-desktop"></i>
                                                <span class="menu-text"> {{ trans('main.users') }}</span>
                                            </a>
                                            <b class="arrow"></b>
                                        </li>
                                        @endpermission

                                    </ul>
                                </li>


                                @if(isset($module_menus))
                                    @foreach($module_menus as $module_menu)
                                        <li class="@if(($module_menu['child']>""&&in_array(Request::segment(2),$module_menu['child'])||Request::segment(2)==$module_menu['name'])) active open @endif">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="menu-icon fa fa-user"></i>
                                                <span class="menu-text"> {{$module_menu['name'].' module'}}</span>
                                                <b class="arrow fa fa-angle-down"></b>
                                            </a>
                                            <b class="arrow"></b>
                                            <ul class="submenu">
                                                <li class="@if(Request::segment(2)==$module_menu['name']) active @endif">
                                                    <a href="{{url('/admin/'.$module_menu['name'])}}">
                                                        <i class="menu-icon fa fa-tachometer"></i>
                                                        <span class="menu-text"> {{ $module_menu['name'] }} </span>
                                                    </a>
                                                    <b class="arrow"></b>
                                                </li>
                                                @if($module_menu['child']>"")
                                                    @foreach($module_menu['child'] as $val)
                                                        <li class="@if(Request::segment(2)==$val) active @endif">
                                                            <a href="{{url('/admin/'.$val)}}">
                                                                <i class="menu-icon fa fa-tachometer"></i>
                                                                <span class="menu-text"> {{ $val }} </span>
                                                            </a>
                                                            <b class="arrow"></b>
                                                        </li>
                                                    @endforeach

                                                @endif
                                            </ul>
                                        </li>
                                    @endforeach
                                @endif
                                <li class="separate-menu"><span>{{ trans('main.sample_module') }}</span></li>
                                <li class="@if(Request::segment(2)=='dashboard') active @endif">
                                    <a href="{{url('/admin/dashboard')}}">
                                        <i class="menu-icon fa fa-tachometer"></i>
                                        <span class="menu-text"> {{ trans('main.dashboard') }} </span>
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                @permission('view_news')
                                <li class="@if(Request::segment(2)=='news') active @endif">
                                    <a href="{{url('/admin/news')}}">
                                        <i class="menu-icon fa fa-pencil-square-o"></i>
                                        <span class="menu-text"> {{ trans('main.news') }} </span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('view_news_category')
                                <li class="@if(Request::segment(2)=='news_category') active @endif">
                                    <a href="{{url('/admin/news_category')}}">
                                        <i class="menu-icon fa fa-pencil-square-o"></i>
                                        <span class="menu-text"> {{ trans('main.news_category') }} </span>
                                    </a>
                                </li>
                                @endpermission
								@permission('add_module')
                                <li class="@if(Request::segment(2)=='modules') active @endif">
                                    <a href="{{url('/admin/modulesbuilder')}}">
                                        <i class="menu-icon fa fa-pencil-square-o"></i>
                                        <span class="menu-text">  Module builder </span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('view_relation')
                                <li class="@if(Request::segment(2)=='relation') active @endif">
                                    <a href="{{url('/admin/relation')}}">
                                        <i class="menu-icon fa fa-exchange"></i>
                                        <span class="menu-text"> Relation </span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('view_ReportBuilder')
                                <li class="@if(Request::segment(2)=='reportbuilders') active @endif">
                                    <a href="{{url('/admin/reportbuilders')}}">
                                        <i class="menu-icon fa fa-line-chart"></i>
                                        <span class="menu-text">  Report builders </span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('view_activity_log')
                                <li class="@if(Request::segment(2)=='activitylogs') active @endif">
                                    <a href="{{url('/admin/activitylogs')}}">
                                        <i class="menu-icon fa fa-history"></i>
                                        <span class="menu-text"> Activity Logs </span>
                                    </a>
                                </li>
                                @endpermission
                                


                            </ul>


                            <a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test">
                                <i id="icon-sw-s-b" class="fa fa-angle-double-left"></i>
                            </a>
                        </div>
                    </div>
                    
                </div><!-- end tab-content-->
            </div><!-- end tabbable-line -->
        </div><!-- end tabbable-panel -->
    </div>
    <!-- /end #sidebar -->

    <!-- main content  -->
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url("admin/dashboard")}}">Home</a>
                    </li>
                    <li>
                        <a href="#">{{Request::segment(2)}}</a>
                    </li>
                </ul>

            </div>

            <!-- main content -->
            <div id="content">
                <!-- if you want active dragable panel, you should add #sortable-panel. handler drag-drop configured on .panel -->
                <div id="" class="">
                    <div id="titr-content" class="col-md-12">
                        <h2>{{ucfirst(Request::segment(2))}}</h2>
                        <h5>Edit and add {{ucfirst(Request::segment(2))}}</h5>
                    </div>

                    <!-- page content load-->
                    @yield('content')
                            <!--/page content load-->

                </div><!-- end col-md-12 -->
            </div>
            <!-- end #content -->

        </div><!-- end .row -->
    </div>
    <!-- ./end #main  -->

    <!-- footer -->
    <div class="page-footer">
        <div class="col-xs-12 col-sm-12 text-center">
            <strong class=""><a href="http://www.techontouch.com">TechOnTouch</a>© 2017</strong>
            <a href="#">
                <i class="fa fa-twitter-square bigger-120"></i>
            </a>
            <a href="#">
                <i class="fa fa-facebook-square bigger-120"></i>
            </a>
            <a href="#">
                <i class="fa fa-rss-square orange bigger-120"></i>
            </a>
        </div>
    </div>
    <!-- /footer -->
</div>
<!-- end #container -->

<!-- Modal mydelete -->
<div class="modal fade" id="mydelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <form>
        <div class="modal-dialog" role="document">
            <input type="hidden" name="delete_value" id="delete_value">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">Are you sure to delete this item?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete_content"><span class="fa fa-trash"></span> Delete </button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- General JS script library-->

<script type="text/javascript" src="{{url('assets/vendors/angularjs/js/angular.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/angularjs/js/angular-sanitize.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/jquery-searchable/js/jquery.searchable.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/underscore/js/underscore.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/restangular/restangular.min.js')}}"></script> <!-- it's dependent to underscore.js -->

<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
<script type="text/javascript" src="{{url('assets/js/yep-script.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/yep-demo.min.js')}}"></script>

<script type="text/javascript" src="{{url('assets/vendors/jquery-require/jquery.require.min.js')}}"></script>

<!-- Related JavaScript Library to This Pagee -->
{{--<script type="text/javascript" src="{{url('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script><!-- Use for input mask -->--}}
<script type="text/javascript" src="{{url('assets/vendors/nprogress/js/nprogress.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/dropzone/js/dropzone.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/ckeditor/js/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/morrisjs/js/raphael.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/morrisjs/js/morris.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/easy-pie-chart/js/jquery.easypiechart.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/select2/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/plugin.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/ui-select/js/select.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/angular-wizard/js/angular-wizard.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/angular-ui-notification/js/angular-ui-notification.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/ng-sweet-alert/SweetAlert.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/ui-bootstrap/js/ui-bootstrap-custom-tpls.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/bootstrap-duallistbox/js/jquery.bootstrap-duallistbox.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/angular-bootstrap-duallistbox/angular-bootstrap-duallistbox.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/momentjs/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/bootstrap-daterangepicker/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/vendors/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>


<!-- Yeptemplate Vendors JS Script --><!-- Please use *.min.js in production -->
<script type="text/javascript" src="{{url('assets/js/yep-vendors.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/yep-custom.js')}}"></script>

<script type="text/javascript" src="{{url('assets/js/app.js')}}"></script>

</body>
</html>