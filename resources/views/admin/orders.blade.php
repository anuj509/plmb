@extends('master')
@section('content')
<div id="ajax_div">

	 <div class="col-md-12">
	 @foreach($orders as $key => $item)
	 	<div class="panel-group yep-accordion no-padding" id="accordion{{$key}}">

										<div class="panel panel-default">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion{{$key}}" data-target="#collapse{{$key}}" >
												<h4 class="panel-title">
													<a class="collapsed">
														<i class="fa fa-lg fa-angle-down pull-right"></i>
														<pre> Order No : <b>{{$item->order_no}}</b> 	  Sub Total : <b>Rs. {{$item->sub_total}}</b>   Customer Name : <b>{{$customers[$key]->customer_name}}</b>  Customer Email : <b>{{$customers[$key]->email}}</b></pre>
													</a>
												</h4>
											</div>
											<div id="collapse{{$key}}" class="panel-collapse collapse">
												<div class="panel-body">
													<table class="table table-striped">
												<tr>
												<th>Product Name  </th> <th> Product Image</th>  <th> Product Price </th> <th> Quantity</th>
												</tr>

												@foreach($orderdetails[$key] as $k => $v)

													<tr>
													<td><?php if(($v->rental)=='true'){ $img=($prods[$key][$k]->rental_product_images); echo $prods[$key][$k]->rental_product_name; }else{ $img=$prods[$key][$k]->product_images; echo $prods[$key][$k]->product_name; } ?></td>

													<td><img src="{{url('uploads')}}/{{json_decode($img,true)[0] }}" height="80px" width="auto"></td>
													<td>{{$v->product_price}}</td>
													<td>{{$v->quantity}}</td>
													</tr>
												@endforeach

												</table>
												</div>
											</div>
										</div>

									</div>
									@endforeach
	 </div>
</div>
@endsection
