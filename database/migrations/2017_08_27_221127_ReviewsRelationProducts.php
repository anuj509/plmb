<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class ReviewsRelationProducts extends Migration
         {
  public function up()
                {   Schema::table('reviews', function($table) {            $table->integer('products_id')->nullable()->unsigned();
            $table->foreign('products_id')->references("id")->on('products')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('reviews', function(Blueprint $table){
                $table->dropForeign(['products_id']);
               $table->dropColumn('products_id');
         });
             }
      }