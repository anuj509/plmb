<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class RentalproductsRelationRentalcategories extends Migration
         {
  public function up()
                {   Schema::table('rentalproducts', function($table) {            $table->integer('rentalcategories_id')->nullable()->unsigned();
            $table->foreign('rentalcategories_id')->references("id")->on('rentalcategories')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('rentalproducts', function(Blueprint $table){
                $table->dropForeign(['rentalcategories_id']);
               $table->dropColumn('rentalcategories_id');
         });
             }
      }