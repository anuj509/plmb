<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class RentalproductsRelationRentalsubcategories extends Migration
         {
  public function up()
                {   Schema::table('rentalproducts', function($table) {            $table->integer('rentalsubcategories_id')->nullable()->unsigned();
            $table->foreign('rentalsubcategories_id')->references("id")->on('rentalsubcategories')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('rentalproducts', function(Blueprint $table){
                $table->dropForeign(['rentalsubcategories_id']);
               $table->dropColumn('rentalsubcategories_id');
         });
             }
      }