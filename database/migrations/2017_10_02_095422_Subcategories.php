<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Subcategories extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subcategory_name', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Subcategories","description" =>"sub categories module","link_name" => "subcategories","status"=>1,"created_at"=>"2017-10-02 09:54:22")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Subcategories','display_name' => 'view_Subcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Subcategories','display_name' => 'add_Subcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Subcategories','display_name' => 'edit_Subcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Subcategories','display_name' => 'delete_Subcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Subcategories')->delete();
        DB::table('permissions')->where('name',  'add_Subcategories')->delete();
        DB::table('permissions')->where('name',  'edit_Subcategories')->delete();
        DB::table('permissions')->where('name',  'delete_Subcategories')->delete();
        ######remove primary key
        Schema::drop('subcategories');
     #####end_down_function#####
    }
}
