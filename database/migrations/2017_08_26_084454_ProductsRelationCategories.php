<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class ProductsRelationCategories extends Migration
         {
  public function up()
                {   Schema::table('products', function($table) {            $table->integer('categories_id')->nullable()->unsigned();
            $table->foreign('categories_id')->references("id")->on('categories')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('products', function(Blueprint $table){
                $table->dropForeign(['categories_id']);
               $table->dropColumn('categories_id');
         });
             }
      }