<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Reviews extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment', 100);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Reviews","description" =>"product reviews module","link_name" => "reviews","status"=>1,"created_at"=>"2017-08-27 22:09:27")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Reviews','display_name' => 'view_Reviews')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Reviews','display_name' => 'add_Reviews')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Reviews','display_name' => 'edit_Reviews')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Reviews','display_name' => 'delete_Reviews')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Reviews')->delete();
        DB::table('permissions')->where('name',  'add_Reviews')->delete();
        DB::table('permissions')->where('name',  'edit_Reviews')->delete();
        DB::table('permissions')->where('name',  'delete_Reviews')->delete();
        ######remove primary key
        Schema::drop('reviews');
     #####end_down_function#####
    }
}
