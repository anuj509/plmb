<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Sliders extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->enum('isactive', ['yes', 'no']);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Sliders","description" =>"slider crud","link_name" => "sliders","status"=>1,"created_at"=>"2017-08-25 19:51:10")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Sliders','display_name' => 'view_Sliders')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Sliders','display_name' => 'add_Sliders')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Sliders','display_name' => 'edit_Sliders')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Sliders','display_name' => 'delete_Sliders')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Sliders')->delete();
        DB::table('permissions')->where('name',  'add_Sliders')->delete();
        DB::table('permissions')->where('name',  'edit_Sliders')->delete();
        DB::table('permissions')->where('name',  'delete_Sliders')->delete();
        ######remove primary key
        Schema::drop('sliders');
     #####end_down_function#####
    }
}
