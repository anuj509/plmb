<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Rentalsubcategories extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('rentalsubcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rental_subcategory_name', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Rentalsubcategories","description" =>"rental sub categories module","link_name" => "rentalsubcategories","status"=>1,"created_at"=>"2017-10-02 13:13:30")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Rentalsubcategories','display_name' => 'view_Rentalsubcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Rentalsubcategories','display_name' => 'add_Rentalsubcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Rentalsubcategories','display_name' => 'edit_Rentalsubcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Rentalsubcategories','display_name' => 'delete_Rentalsubcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Rentalsubcategories')->delete();
        DB::table('permissions')->where('name',  'add_Rentalsubcategories')->delete();
        DB::table('permissions')->where('name',  'edit_Rentalsubcategories')->delete();
        DB::table('permissions')->where('name',  'delete_Rentalsubcategories')->delete();
        ######remove primary key
        Schema::drop('rentalsubcategories');
     #####end_down_function#####
    }
}
