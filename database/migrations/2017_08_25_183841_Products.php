<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Products extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name', 100);
            $table->text('product_images');
            $table->text('description');
            $table->double('base_price');
            $table->enum('discount_available', ['yes', 'no']);
            $table->double('discounted_price');
            $table->enum('isnew', ['yes', 'no']);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Products","link_name" => "products","status"=>1,"created_at"=>"2017-08-25 18:38:41")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Products','display_name' => 'view_Products')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Products','display_name' => 'add_Products')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Products','display_name' => 'edit_Products')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Products','display_name' => 'delete_Products')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Products')->delete();
        DB::table('permissions')->where('name',  'add_Products')->delete();
        DB::table('permissions')->where('name',  'edit_Products')->delete();
        DB::table('permissions')->where('name',  'delete_Products')->delete();
        ######remove primary key
        Schema::drop('products');
     #####end_down_function#####
    }
}
