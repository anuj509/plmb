<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Customers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name', 30);
            $table->string('email', 30);
            $table->string('phone', 30);
            $table->string('password', 100);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Customers","description" =>"customers module","link_name" => "customers","status"=>1,"created_at"=>"2017-08-26 17:55:48")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Customers','display_name' => 'view_Customers')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Customers','display_name' => 'add_Customers')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Customers','display_name' => 'edit_Customers')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Customers','display_name' => 'delete_Customers')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Customers')->delete();
        DB::table('permissions')->where('name',  'add_Customers')->delete();
        DB::table('permissions')->where('name',  'edit_Customers')->delete();
        DB::table('permissions')->where('name',  'delete_Customers')->delete();
        ######remove primary key
        Schema::drop('customers');
     #####end_down_function#####
    }
}
