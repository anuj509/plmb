<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class ProductsRelationSubcategories extends Migration
         {
  public function up()
                {   Schema::table('products', function($table) {            $table->integer('subcategories_id')->nullable()->unsigned();
            $table->foreign('subcategories_id')->references("id")->on('subcategories')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('products', function(Blueprint $table){
                $table->dropForeign(['subcategories_id']);
               $table->dropColumn('subcategories_id');
         });
             }
      }