<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class ReviewsRelationCustomers extends Migration
         {
  public function up()
                {   Schema::table('reviews', function($table) {            $table->integer('customers_id')->nullable()->unsigned();
            $table->foreign('customers_id')->references("id")->on('customers')->onDelete("cascade");
                     });
                }    public function down(){
             Schema::table('reviews', function(Blueprint $table){
                $table->dropForeign(['customers_id']);
               $table->dropColumn('customers_id');
         });
             }
      }