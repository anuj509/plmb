<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Image
	</label>
	<div class="col-sm-10">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="image" ></div>
		<input name="image" type="hidden" value="<?php echo e(isset($editSliders['image']) ? $editSliders['image'] : ''); ?>">
	</div>
</div>

<!-- Isactive Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        <?php echo Form::label('isactive', 'Isactive:'); ?>

    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editSliders['isactive'])&&$editSliders['isactive']=="yes")||!isset($editSliders['isactive']) ? 'checked' : ''); ?> id="active" value="yes" type="radio" name="isactive"   class="">
    <label for="Isactive"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editSliders['isactive'])&&$editSliders['isactive']=="no")||!isset($editSliders['isactive']) ? 'checked' : ''); ?> id="active" value="no" type="radio" name="isactive"   class="">
    <label for="Isactive"> no</label>
</div>


    </div>
</div>
