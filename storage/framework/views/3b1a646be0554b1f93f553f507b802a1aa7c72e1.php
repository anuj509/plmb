<?php $__env->startSection('content'); ?>
<div id="ajax_div">

	 <div class="col-md-12">
	 <?php foreach($orders as $key => $item): ?>
	 	<div class="panel-group yep-accordion no-padding" id="accordion<?php echo e($key); ?>">

										<div class="panel panel-default">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion<?php echo e($key); ?>" data-target="#collapse<?php echo e($key); ?>" >
												<h4 class="panel-title">
													<a class="collapsed">
														<i class="fa fa-lg fa-angle-down pull-right"></i>
														<pre> Order No : <b><?php echo e($item->order_no); ?></b> 	  Sub Total : <b>Rs. <?php echo e($item->sub_total); ?></b>   Customer Name : <b><?php echo e($customers[$key]->customer_name); ?></b>  Customer Email : <b><?php echo e($customers[$key]->email); ?></b></pre>
													</a>
												</h4>
											</div>
											<div id="collapse<?php echo e($key); ?>" class="panel-collapse collapse">
												<div class="panel-body">
													<table class="table table-striped">
												<tr>
												<th>Product Name  </th> <th> Product Image</th>  <th> Product Price </th> <th> Quantity</th>
												</tr>

												<?php foreach($orderdetails[$key] as $k => $v): ?>

													<tr>
													<td><?php if(($v->rental)=='true'){ $img=($prods[$key][$k]->rental_product_images); echo $prods[$key][$k]->rental_product_name; }else{ $img=$prods[$key][$k]->product_images; echo $prods[$key][$k]->product_name; } ?></td>

													<td><img src="<?php echo e(url('uploads')); ?>/<?php echo e(json_decode($img,true)[0]); ?>" height="80px" width="auto"></td>
													<td><?php echo e($v->product_price); ?></td>
													<td><?php echo e($v->quantity); ?></td>
													</tr>
												<?php endforeach; ?>

												</table>
												</div>
											</div>
										</div>

									</div>
									<?php endforeach; ?>
	 </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>