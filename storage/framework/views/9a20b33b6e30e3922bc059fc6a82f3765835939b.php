<!-- Customer Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Customer Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCustomers['customer_name']) ? $editCustomers['customer_name'] : ''); ?>" name="customer_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Email Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Email
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCustomers['email']) ? $editCustomers['email'] : ''); ?>" name="email" type="email" placeholder="" class="form-control">
	</div>
</div>

<!-- Phone Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Phone
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCustomers['phone']) ? $editCustomers['phone'] : ''); ?>" name="phone" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Password Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Password
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCustomers['password']) ? $editCustomers['password'] : ''); ?>" name="password" type="password" placeholder="" class="form-control">
	</div>
</div>
