<!-- Comment Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Comment
	</label>
	<div class="col-sm-9">
		<textarea  name="comment" type="text" placeholder="" class="form-control"><?php echo e(isset($editReviews['comment']) ? $editReviews['comment'] : ''); ?></textarea>
	</div>
</div>
