<td data-title="Category_name"><?php foreach($Categories as $key=>$val): ?>
<?php if($content->categories_id==$key): ?>
<?php echo e($val); ?>

<?php endif; ?>
<?php endforeach; ?>
</td>
<td data-title="SubCategory_name"><?php foreach($Subcategories as $key=>$val): ?>
<?php if($content->subcategories_id==$key): ?>
<?php echo e($val); ?>

<?php endif; ?>
<?php endforeach; ?>
</td>
<td data-title="product_name"><?php echo e($content->product_name); ?></td>

<td data-title="product_images"><img class="img-thumbnail" src="../uploads/<?php echo e(json_decode($content->product_images,true)[0]); ?>" /></td>

<td data-title="description"><?php echo e($content->description); ?></td>

<td data-title="base_price"><?php echo e($content->base_price); ?></td>

<td data-title="discount_available"><?php echo e($content->discount_available); ?></td>

<td data-title="discounted_price"><?php echo e($content->discounted_price); ?></td>

<td data-title="isnew"><?php echo e($content->isnew); ?></td>

<td data-title="isfeatured"><?php echo e($content->isfeatured); ?></td>
