


<!DOCTYPE HTML>
<html>
<head>
<title>Naval | Cart</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Flat Cart Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<!--google fonts-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

</head>
<body style="background: #387296">
<div class="logo">
	<h3>Naval Services</h3>
</div>
<div class="cart">
   <div class="cart-top">
   	  <div class="cart-experience">
   	  	 <h4>Shopping Cart Experience</h4>
   	  </div>
   	  <div class="cart-login">
   	  	 <div class="cart-login-img">

   	  	 </div>
   	  	 <div class="cart-login-text">
   	  	 	<h5>Logged in as</h5>
   	  	 </div>

          <div class="cart-login-text">
            <h5><?php echo e($customer_name); ?></h5>
          </div>

   	  	 <div class="clear"> </div>
   	  </div>
   	 <div class="clear"> </div>
   </div>
   <div class="cart-bottom">
   	 <div class="table">
   	 	<table>
   	 		<tbody>
   	 	     <?php if(isset($cartcont)): ?>
   	 	      <tr  class="main-heading">
		 			<th>Images</th>
		 			<th class="long-txt">Product Description</th>
		 			<th>Quantity</th>
		 			<th>Price</th>
		 			<th>Total</th>
		 			<th>Action</th>
   	 	     </tr>


   	 	     <?php foreach($cartcont as $key => $item): ?>

					 <?php if(($item->rental)=='true'){ $rental=true; }else{ $rental=false; } ?>
					 <tr class="cake-top">
   	 	     	<td class="cakes">
							<?php echo (($item->rental)=='true')?'(Rental)':''; ?>
   	 	     		<div class="product-img">
   	 	     			<img src="<?php echo e(url('/uploads')); ?>/<?php echo e(json_decode((($rental)? $products[$key]->rental_product_images:$products[$key]->product_images),true)[0]); ?>" height="80px" width="auto">
   	 	     		</div>
   	 	        </td>
   	 	        <td class="cake-text">
   	 	     		<div class="product-text">
   	 	     			<h3><?php echo e((($rental)? $products[$key]->rental_product_name:$products[$key]->product_name)); ?></h3>

   	 	     		</div>
 	     	    </td>
 	     	    <td class="quantity">

   	 	     	  	 <form method="post" action="/cart/update/<?php echo e($item->product_id); ?>">
					<div class="product-right">
   	 	     	  	 	<input type="number" name="quantity" id="quantity" value="<?php echo e($item->quantity); ?>" class="form-control input-small"><?php echo e(csrf_field()); ?><button type="submit" class="shop">update</button>
   	 	     	  	 </div>
   	 	     	  	 </form>

   	 	     	</td>
   	 	     	<td class="price">
   	 	     		<h4>₹<?php echo e(explode('.',$item->product_price)[0]); ?></h4>
   	 	     	</td>
   	 	     	<td><h4>₹<?php echo explode('.',$item->product_price)[0]*($item->quantity); ?></h4>
   	 	     	</td>
   	 	     	<td class="top-remove">
   	 	     		<form method="post" action="/cart/delete/<?php echo e($item->product_id); ?>"><?php echo e(csrf_field()); ?>

   	 	     		<div class="close">
   	 	     	      <h5><input class="del" type="submit" value="delete"></h5>
   	 	            </div>
   	 	            </form>
   	 	     	</td>

   	 	     </tr>
   	 	      <?php endforeach; ?>
   	 	      <?php else: ?>
   	 	      <tr>No Items In Cart</tr>
   	 	      <?php endif; ?>
   	 	   </tbody>
   	 	</table>
   	 </div>
   	 <?php if(isset($cart)): ?>
   	 <div class="vocher">
   	 	<div class="tot-btn">

               <form method="post" action="/cart/delete">
						<?php echo e(csrf_field()); ?>

						<button type="submit" class="shop">Delete Cart</button>
				</form>
				<form method="post" action="/inquirysend"><?php echo e(csrf_field()); ?><button type="submit" class="check">Inquire Now</button>
            </div>

   	 	<div class="dis-total">
   	 		<h1>Total ₹<?php echo e($cart->sub_total); ?></h1>

   	 	</div>
   	   <div class="clear"> </div>
   	 </div>
   	 <?php endif; ?>
   </div>
</div>
<div class="copy-right">
			<p>2017 Naval Services. All rights reserved | Designed by  <a href="http://techontouch.com/" target="_blank">  TechOnTouch </a></p>
</div>

</body>
</html>
