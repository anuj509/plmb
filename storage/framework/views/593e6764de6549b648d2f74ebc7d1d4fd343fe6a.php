<section class="our-gallery" id="clients">
    <h3 class="text-center slideanim">Our Clients</h3>
    <p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 
    <div class="container">
        <div class="content slideanim">
            <div class="chroma-gallery mygallery">
                <?php foreach($clients as $client): ?>
                <img src="uploads/<?php echo e($client->client_image); ?>" alt="<?php echo e($client->client_name); ?>" data-largesrc="<?php echo e($client->client_image); ?>">
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>