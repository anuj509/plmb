<?php foreach($products->chunk(4) as $chunk): ?>
<div class="single-pro">
<?php foreach($chunk as $product): ?>
			<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="<?php echo e(url('/uploads')); ?>/<?php echo e(json_decode((($rental)? $product->rental_product_images:$product->product_images),true)[0]); ?>" alt="<?php echo e((($rental)? $product->rental_product_name:$product->product_name)); ?>" class="pro-image-front" height="250px">
										<img src="<?php echo e(url('/uploads')); ?>/<?php echo e(json_decode((($rental)? $product->rental_product_images:$product->product_images),true)[0]); ?>" alt="<?php echo e((($rental)? $product->rental_product_name:$product->product_name)); ?>" class="pro-image-back">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="<?php echo e((($rental)? url('/rentalcategories'):url('/categories'))); ?>/<?php echo e(Request::segment(2)); ?>/product/<?php echo e($product->id); ?>" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<?php if($product->isnew=='yes'): ?>
											<span class="product-new-top">New</span>
											<?php endif; ?>
									</div>
									<div class="item-info-product ">
										<h4><a href="<?php echo e(url('/categories')); ?>/<?php echo e(Request::segment(2)); ?>/product/<?php echo e($product->id); ?>"><?php echo e((($rental)? $product->rental_product_name:$product->product_name)); ?></a></h4>
										<div class="info-product-price">
											<?php if($product->discount_available=='yes'): ?>
											<span class="item_price">Rs. <?php echo e($product->discounted_price); ?></span>
											<del>Rs. <?php echo e($product->base_price); ?></del>
											<?php else: ?>
											<span class="item_price">Rs. <?php echo e($product->base_price); ?></span>
											<?php endif; ?>
										</div>
										<form action="/cart/add/<?php echo e($product->id); ?>" method="post">
										<input class="input-lg thumbnail form-control" type="number"  min="1" max="10000000000" name="quantity" id="quantity" value="1" style="width:100%" placeholder="Specify Quantity" required>
										<input type="hidden" name="rental" value="<?php echo ($rental)?'true':'false';?>">
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
														<?php echo e(csrf_field()); ?>

																<fieldset>
																	<input type="submit" name="submit" value="Inquire Now" class="button">
																</fieldset>

														</div>
										</form>
									</div>
								</div>
							</div>
<?php endforeach; ?>
							<div class="clearfix"></div>
		</div>
<?php endforeach; ?>
