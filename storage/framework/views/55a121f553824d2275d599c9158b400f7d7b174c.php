<!DOCTYPE html>
<html>
<head>
<title>Collegiate a Education category Flat Bootstrap Responsive Website Template | :: W3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Exchange Education a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- css files -->
<link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo e(asset('css/chromagallery.css')); ?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo e(asset('css/bootstrap.css')); ?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo e(asset('css/style1.css')); ?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo e(asset('css/font-awesome.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/easy-responsive-tabs.css')); ?>" rel='stylesheet' type='text/css'/>
<!-- /css files -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Viga' rel='stylesheet' type='text/css'>
<!-- /fonts -->
<!-- js files -->
<script src="<?php echo e(asset('js/modernizr.custom.js')); ?>"></script>
<!-- /js files -->
</head>
<body>
<!-- header -->
<div class="header" id="home">

</div>
<!-- //header -->
<!-- header-bot -->
<div class="header-bot">
	<div class="header-bot_inner_wthreeinfo_header_mid">
		<div class="col-md-4 header-middle">
			<form action="#" method="post">
					<input type="search" name="search" placeholder="Search here..." required="">
					<input type="submit" value=" ">
				<div class="clearfix"></div>
			</form>
		</div>
		<!-- header-bot -->
			<div class="col-md-4 logo_agile">
				<h1><a href="index.html"><span>N</span>aval Service <i class="fa fa-shopping-bag top_logo_agile_bag" aria-hidden="true"></i></a></h1>
			</div>
        <!-- header-bot -->
		<div class="col-md-4 agileits-social top_content">




		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //header-bot -->
<!-- banner -->
<?php echo $__env->make('front_end.nav1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- //banner-top -->

<!-- /banner_bottom_agile_info -->
<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3><?php echo ($rental)?'Rental ':''; ?>PRODUCTS</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
						<div class="agile_inner_breadcrumb">

						   <ul class="w3_short">
								<li><a href="index.html">Home</a><i>|</i></li>
								<li>Products</li>
							</ul>
						 </div>
				</div>
	   <!--//w3_short-->
	</div>
</div>

  <!-- banner-bootom-w3-agileits -->
	<div class="banner-bootom-w3-agileits">
	<div class="container">
         <!-- mens -->
		<div class="col-md-4 products-left">

			<div class="css-treeview">
				<h4><?php echo ($rental)?'Rental ':''; ?>Categories</h4>
				<ul class="tree-list-pad">
							<!-- <li><label for="item-0"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Products</label> -->
							<?php foreach($subcategories as $subcategory): ?>
							<?php if((Request::segment(4))==(($rental)? (rawurlencode($subcategory->rental_subcategory_name)):(rawurlencode($subcategory->subcategory_name)))): ?>
							<li><label for="item-0"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <?php echo e((($rental)? ($subcategory->rental_subcategory_name):($subcategory->subcategory_name))); ?></label></li>
							<?php else: ?>
							<li><a href="<?php echo e((($rental)? url('/rentalcategories'):url('/categories'))); ?>/<?php echo e(Request::segment(2)); ?>/<?php echo e(Request::segment(3)); ?>/<?php echo e((($rental)? ($subcategory->rental_subcategory_name):($subcategory->subcategory_name))); ?>/<?php echo e((($rental)? ($subcategory->id):($subcategory->id))); ?>"><?php echo e((($rental)? ($subcategory->rental_subcategory_name):($subcategory->subcategory_name))); ?></a></li>
							<?php endif; ?>
							<?php endforeach; ?>
				</ul>
			</div>
			<div class="community-poll">
				<h4>Bank Details</h4>
				<div class="swit form">
					<form>
					<div> <div> <label><i></i>More convenient for shipping and delivery</label> </div></div>
					<div > <div > <label><i></i>Lower Price</label> </div></div>
					<div class="check_box"> <div> <label><i></i>Track your item</label> </div></div>
					<div class="check_box"> <div> <label><i></i>Bigger Choice</label> </div></div>
					<div class="check_box"> <div> <label><i></i>More colors to choose</label> </div></div>
					<div class="check_box"> <div> <label><i></i>Secured Payment</label> </div></div>
					<div class="check_box"> <div> <label><i></i>Money back guaranteed</label> </div></div>
					<div class="check_box"> <div> <label><i></i>Others</label> </div></div>

					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-8 products-right">
			<h5>Product <span>Compare(0)</span></h5>


			<div class="men-wear-bottom">
				<div class="col-sm-4 men-wear-left">
					<img class="img-responsive" src="<?php echo e(url('/images/Plasma TV.jpg')); ?>" alt=" " />
				</div>
				<div class="col-sm-8 men-wear-right">
					<h4> <span>LED TV</span></h4>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
					accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
					ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
					explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
					odit aut fugit. </p>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if(isset($featuredproducts)): ?>
			<?php foreach($featuredproducts as $featuredproduct): ?>
			<div class="col-md-4 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="<?php echo e(url('/uploads')); ?>/<?php echo e(json_decode((($rental)? $featuredproduct->rental_product_images:$featuredproduct->product_images),true)[0]); ?>" alt="<?php echo e((($rental)? $featuredproduct->rental_product_name:$featuredproduct->product_name)); ?>" class="pro-image-front img-square">
										<img src="<?php echo e(url('/uploads')); ?>/<?php echo e(json_decode((($rental)? $featuredproduct->rental_product_images:$featuredproduct->product_images),true)[1]); ?>" alt="<?php echo e((($rental)? $featuredproduct->rental_product_name:$featuredproduct->product_name)); ?>" class="pro-image-back img-square">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="<?php echo e((($rental)? url('/rentalcategories'):url('/categories'))); ?>/<?php echo e(Request::segment(2)); ?>/product/<?php echo e($featuredproduct->id); ?>" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<?php if($featuredproduct->isnew=='yes'): ?>
											<span class="product-new-top">New</span>
											<?php endif; ?>

									</div>
									<div class="item-info-product ">
										<h4><a href="<?php echo e((($rental)? url('/rentalcategories'):url('/categories'))); ?>/<?php echo e(Request::segment(2)); ?>/product/<?php echo e($featuredproduct->id); ?>"><?php echo e((($rental)? $featuredproduct->rental_product_name:$featuredproduct->product_name)); ?></a></h4>
										<div class="info-product-price">
											<?php if($featuredproduct->discount_available=='yes'): ?>
											<span class="item_price">Rs. <?php echo e($featuredproduct->discounted_price); ?></span>
											<del>Rs. <?php echo e($featuredproduct->base_price); ?></del>
											<?php else: ?>
											<span class="item_price">Rs. <?php echo e($featuredproduct->base_price); ?></span>
											<?php endif; ?>
										</div>
										<form action="/cart/add/<?php echo e($featuredproduct->id); ?>" method="post">
										<input class="input-lg thumbnail form-control" type="number"  min="1" max="10000000000" name="quantity" id="quantity" value="1" style="width:100%" placeholder="Specify Quantity" required>
										<input type="hidden" name="rental" value="true">
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
														<?php echo e(csrf_field()); ?>

																<fieldset>
																	<input type="submit" name="submit" value="Inquire Now" class="button">
																</fieldset>

														</div>
										</form>

									</div>
								</div>
							</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
<!--products of categorie selected-->
<?php echo $__env->make('front_end.items', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--/products of categorie selected-->
	</div>
</div>
<!-- //mens -->
<!--/grids-->
<
<!--grids-->
<!-- footer -->
<div class="footer">
	<div class="footer_agile_inner_info_w3l">
		<div class="col-md-3 footer-left">
			<h2><a href="index.html"><span>N</span>aval Services </a></h2>
			<p>NAVAL is a well known name in the market and provides services to many customers,
for the goods brought from parallel market or goods sold outside the normal
distribution channel.
The spare parts or replacement parts business of your company will grow through our
services.</p>
			<ul class="social-nav model-3d-0 footer-social w3_agile_social two">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter">
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
		</div>
		<div class="col-md-9 footer-right">
			<div class="sign-grds">
				<div class="col-md-5 sign-gd">
					<h4>Our <span>Information</span> </h4>
					<ul>
						<li><a href="index.html">Home</a></li>
						<li><a href="index.html">About Us</a></li>

						<li><a href="index.html">Services</a></li>

						<li><a href="index.html">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-7 sign-gd-two">
					<h4>Store <span>Information</span></h4>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Phone Number</h6>
								<p>+1 234 567 8901</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email Address</h6>
								<p>Email :<a href="mailto:example@email.com"> mail@example.com</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<p>Broome St, NY 10002,California, USA.

								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
			<div class="agile_newsletter_footer">
					<div class="col-sm-6 newsleft">
				<h3>SIGN UP FOR NEWSLETTER !</h3>
			</div>
			<div class="col-sm-6 newsright">
				<form action="#" method="post">
					<input type="email" placeholder="Enter your email..." name="email" required="">
					<input type="submit" value="Submit">
				</form>
			</div>

		<div class="clearfix"></div>
	</div>
		<p class="copy-right">&copy 2017 Naval Services. All rights reserved | Design by <a href="http://techontouch.com/">TechOnTouch</a></p>
	</div>
</div>
<!-- //footer -->


<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<script type="text/javascript" src="<?php echo e(asset('js/jquery-2.1.4.min.js')); ?>"></script>
<!-- //js -->
<script src="<?php echo e(asset('js/responsiveslides.min.js')); ?>"></script>
				<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
				</script>
<script src="<?php echo e(asset('js/modernizr.custom.js')); ?>"></script>
	<!-- Custom-JavaScript-File-Links -->
	<!---->
							<script type='text/javascript'>//<![CDATA[
							$(window).load(function(){
							 $( "#slider-range" ).slider({
										range: true,
										min: 0,
										max: 9000,
										values: [ 1000, 7000 ],
										slide: function( event, ui ) {  $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
										}
							 });
							$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );

							});//]]>

							</script>
						<script type="text/javascript" src="<?php echo e(asset('js/jquery-ui.js')); ?>"></script>
					 <!---->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo e(asset('js/move-top.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/jquery.easing.min.js')); ?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
<!-- //here ends scrolling icon -->

<!-- for bootstrap working -->
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.js')); ?>"></script>
</body>
</html>
