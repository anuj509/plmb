<!-- Product Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Product Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editProducts['product_name']) ? $editProducts['product_name'] : ''); ?>" name="product_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Product Images
	</label>
	<div class="col-sm-10">
		<div  class="dropzone multi-uploader" id="dropzone" data-name="product_images"></div>
		<input name="product_images" type="hidden" value="<?php echo e(isset($editProducts['product_images']) ? $editProducts['product_images'] : ''); ?>" style="width:100%;">
	</div>
</div>

<!-- Description Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Description
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editProducts['description']) ? $editProducts['description'] : ''); ?>" name="description" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Base Price Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Base Price
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editProducts['base_price']) ? $editProducts['base_price'] : ''); ?>" name="base_price" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Discount Available Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        <?php echo Form::label('discount_available', 'Discount Available:'); ?>

    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['discount_available'])&&$editProducts['discount_available']=="yes")||!isset($editProducts['discount_available']) ? 'checked' : ''); ?> id="active" value="yes" type="radio" name="discount_available"   class="">
    <label for="Discount Available"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['discount_available'])&&$editProducts['discount_available']=="no")||!isset($editProducts['discount_available']) ? 'checked' : ''); ?> id="active" value="no" type="radio" name="discount_available"   class="">
    <label for="Discount Available"> no</label>
</div>


    </div>
</div>

<!-- Discounted Price Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Discounted Price
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editProducts['discounted_price']) ? $editProducts['discounted_price'] : ''); ?>" name="discounted_price" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Isnew Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        <?php echo Form::label('isnew', 'Isnew:'); ?>

    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['isnew'])&&$editProducts['isnew']=="yes")||!isset($editProducts['isnew']) ? 'checked' : ''); ?> id="active" value="yes" type="radio" name="isnew"   class="">
    <label for="Isnew"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['isnew'])&&$editProducts['isnew']=="no")||!isset($editProducts['isnew']) ? 'checked' : ''); ?> id="active" value="no" type="radio" name="isnew"   class="">
    <label for="Isnew"> no</label>
</div>



    </div>
</div>


<!-- Isfeatured Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        <?php echo Form::label('isfeatured', 'Isfeatured:'); ?>

    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['isfeatured'])&&$editProducts['isfeatured']=="yes")||!isset($editProducts['isfeatured']) ? 'checked' : ''); ?> id="active" value="yes" type="radio" name="isfeatured"   class="">
    <label for="Isfeatured"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input <?php echo e((isset($editProducts['isfeatured'])&&$editProducts['isfeatured']=="no")||!isset($editProducts['isfeatured']) ? 'checked' : ''); ?> id="active" value="no" type="radio" name="isfeatured"   class="">
    <label for="Isfeatured"> no</label>
</div>

			</div>
</div>
