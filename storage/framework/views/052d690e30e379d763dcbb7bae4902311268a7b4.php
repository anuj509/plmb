<?php $__env->startSection('content'); ?>
    <section class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <a href="/" class="logo"><h1>Naval</h1></a>
            </div>

        </div>
    </div>
</section>
<!-- /Top Bar -->
<!-- Navigation Bar -->
<?php echo $__env->make('front_end.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- /Navigation Bar -->
<!-- Banner Section -->
 <!-- Carousel
    ================================================== -->
<?php echo $__env->make('front_end.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- /.carousel -->
<!-- /Banner Section -->
<!-- About Section -->
<section class="about-us" id="why">
    <h3 class="text-center slideanim">Why Choose Us</h3>
    <p class="text-center slideanim">We are masters of this field</p>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <img src="images/icon-1.png" class="img-responsive slideanim" alt="about-img">
                        </div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="about-info slideanim">
                                <p>Authorized deals with the end customers through single channel of distribution for sales , installation, maintenance and after sale services of your products.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <img src="images/icon-2.png" class="img-responsive slideanim" alt="about-img">
                        </div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="about-info slideanim">
                                <p>NAVAL is a well known name in the market and provides services to many customers,
for the goods brought from parallel market or goods sold outside the normal
distribution channel.
The spare parts or replacement parts business of your company will grow through our
services.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row below">
            <div class="col-lg-6 col-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <img src="images/icon-3.png" class="img-responsive slideanim" alt="about-img">
                        </div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="about-info slideanim">
                                <p>Target major customers of South Mumbai through the Goodwill of Naval that can be used by you.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <img src="images/icon-4.png" class="img-responsive slideanim" alt="about-img">
                        </div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="about-info slideanim">
                                <p>Regular Business and satisfied customers of your products through our services.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /About Section -->
<!-- Our Services -->
<section class="our-services" id="services">
    <h3 class="text-center slideanim">Our Services</h3>
    <p class="text-center slideanim">Sales, Consultancy, Installations, maintenance and Chip Level repairs of the following household and industrial electronic appliance :</p>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/Plasma-TV.png" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>LCD’s, Projectors, Plasmas</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/amplifier.jpg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>LPRO Amp, All brands of amplifier, Hi-end audio including B&O, Nakamichi , Marantz range.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/conference.jpeg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>Audio / Video units for conference rooms eg :- projectors, plasma etc.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/farmhouse.jpg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>Electronic appliances for Office , Factory and Farmhouse.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/microprocessor.jpg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>Industrial Circuits – Microprocessor & Digital based CKT, A/C ckt for indoor unit as well as A/C
plant.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/miniroom.jpg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>Mini Video Rooms – Complete with High HD Projectors , Mechanised Screens – Pro Amps –
Speakers.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="serv-details">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <img src="images/stereo.jpg" alt="" class="img-responsive slideanim">
                        </div>

                    </div>
                </div>
                <div class="serv-info slideanim">
                    <p>Hi-Fi Gadgets – portable HARD drive based Media Centre, all range of MP3 players.</p>
                </div>
            </div>
            </div>
    </div>
</section>
<!-- /Our Services -->
<!-- Our Information -->
<section class="our-info" id="about">
    <h3 class="text-center slideanim">About Us</h3>
    <p class="text-center slideanim"></p>
    <div class="container">
        <div class="row info-part">
            <div class="col-lg-6 col-md-6 col-sm-6 info-specs">
                <div class="info-details slideanim">
                    <h4>Naval Services, the best you can get</h4>
                    <p>Naval Services has consistently grown from past 22 years in the service industry. The company provides
par excellence
We provide services all across Mumbai and especially in South Mumbai (Colaba, Worli, Marine lines etc). We have a dedicated team of engineer’s expertise in sales; installation and maintenance of the Audio visual Appliances and digital gadgets of all kinds. We have handled large contracts and are extremely equipped regarding the same.</p>

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 info-specs">
                <img src="images/info-img.jpg" alt="" class="img-responsive main-img slideanim">
            </div>
        </div>

    </div>
</section>
<!-- /Our Information -->
<!-- Our Gallery -->
<?php echo $__env->make('front_end.clients', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- /Our Gallery -->
<!-- Our Curriculum -->
<!-- /Our Curriculum -->
<!-- Google Map -->
<section class="map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 slideanim">
                <iframe class="googlemaps" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d380510.6741687111!2d-88.01234121699822!3d41.83390417061058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1455598377120" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<!-- /Google Map -->
<!-- Contact Section -->
<section class="our-contacts" id="contact">
	<h3 class="text-center slideanim">Contact Us</h3>
	<p class="text-center slideanim"></p>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="row">
						<div class="form-group col-lg-4 slideanim">
							<input type="text" class="form-control user-name" placeholder="Your Name" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="email" class="form-control mail" placeholder="Your Email" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="tel" class="form-control pno" placeholder="Your Phone Number" required/>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-lg-12 slideanim">
							<textarea class="form-control" rows="6" placeholder="Your Message" required/></textarea>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<button type="submit" href="#" class="btn-outline1">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- /Contact Section -->
<!-- Footer Section -->
<div class="footer">
	<div class="footer_agile_inner_info_w3l">
		<div class="col-md-3 footer-left">
			<h2><a href="index.html"><span>N</span>aval Services </a></h2>
			<p>NAVAL is a well known name in the market and provides services to many customers,
for the goods brought from parallel market or goods sold outside the normal
distribution channel.
The spare parts or replacement parts business of your company will grow through our
services.</p>
			<ul class="social-nav model-3d-0 footer-social w3_agile_social two">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter">
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
		</div>
		<div class="col-md-9 footer-right">
			<div class="sign-grds">
				<div class="col-md-5 sign-gd">
					<h4>Our <span>Information</span> </h4>
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="#about">About Us</a></li>

						<li><a href="#services">Services</a></li>

						<li><a href="#contact">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-7 sign-gd-two">
					<h4>Store <span>Information</span></h4>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Phone Number</h6>
								<p>+1 234 567 8901</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email Address</h6>
								<p>Email :<a href="mailto:example@email.com"> mail@example.com</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<p>Room No. 5/7, Narayan Nivas, Dhus Wadi, Behind Marine Lines Church, Marine Lines, Mumbai - 400002.

								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
			<div class="agile_newsletter_footer">
					<div class="col-sm-6 newsleft">
				<h3>SIGN UP FOR NEWSLETTER !</h3>
			</div>
			<div class="col-sm-6 newsright">
				<form action="#" method="post">
					<input type="email" placeholder="Enter your email..." name="email" required="">
					<input type="submit" value="Submit">
				</form>
			</div>

		<div class="clearfix"></div>
	</div>
		<p class="copy-right">&copy 2017 Naval Services. All rights reserved | Design by <a href="http://techontouch.com/">TechOnTouch</a></p>
	</div>
</div>
<!-- /Footer Section -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front_end.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>