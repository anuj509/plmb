<html>
	<head>
	<title></title>
	</head>
<body>
	<p>register</p>
	<?php if(count($errors) > 0): ?>
  	<div class="alert alert-danger">
    <?php foreach($errors->all() as $error): ?>
      <p><?php echo e($error); ?></p>
      <?php endforeach; ?>
    </div>
    <?php endif; ?>
	<form method="post" action="/register">

		<input type="text" placeholder="email" name="email" value="<?php echo e(old('email')); ?>">
		<input type="text" placeholder="full name" name="name" value="<?php echo e(old('name')); ?>">
		<input type="text" placeholder="phone" name="phone" value="<?php echo e(old('phone')); ?>">
		<input type="password" placeholder="password" name="password">
		<input type="password" placeholder="confirm password" name="cnfpassword">
		<?php echo e(csrf_field()); ?>

		<input type="submit">
	</form>
</body>	
</html>