<div class="ban-top">
	<div class="container">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					<li class="active menu__item"><a class="menu__link" href="<?php echo e(url('/')); ?>">Home <span class="sr-only">(current)</span></a></li>
					<li class=" menu__item"><a class="menu__link" href="<?php echo e(url('/#about')); ?>">About</a></li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div class="agile_inner_drop_nav_info">

									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<?php foreach($categories as $category): ?>
											<li><a href="<?php echo e(url('/categories')); ?>/<?php echo e(strtolower($category->category_name)); ?>/<?php echo e($category->id); ?>"><?php echo e($category->category_name); ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>

									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rentals<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div class="agile_inner_drop_nav_info">

									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<?php foreach($rentalcategories as $rentalcategory): ?>
											<li><a href="/rentalcategories/<?php echo e(strtolower($rentalcategory->rental_category_name)); ?>/<?php echo e($rentalcategory->id); ?>"><?php echo e($rentalcategory->rental_category_name); ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>

									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
					<li class="menu__item dropdown">
					   <a class="menu__link" href="<?php echo e(url('/#services')); ?>">Our Services</a>
					</li>
					<li class=" menu__item"><a class="menu__link" href="<?php echo e(url('/#contact')); ?>">Contact</a></li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo (session()->has('customersession'))? session()->get('customersession')->customer_name:'Account';?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div class="agile_inner_drop_nav_info">

									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<?php if(session()->has('customersession')): ?>
											<li><a href="<?php echo e(url('/cart')); ?>">Cart</a></li>
											<li><a href="<?php echo e(url('/logout')); ?>">LogOut</a></li>
											<?php else: ?>
											<li><a href="<?php echo e(url('/login')); ?>">Login/SignUp</a></li>
											<?php endif; ?>
										</ul>
									</div>

									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
				  </ul>
				</div>
			  </div>
			</nav>
		</div>

		<div class="clearfix"></div>
	</div>
</div>
