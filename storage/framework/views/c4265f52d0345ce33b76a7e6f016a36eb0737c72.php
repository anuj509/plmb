<!DOCTYPE HTML>
<html>
<head>
<title>Naval | Cart</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Flat Cart Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<!--google fonts-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.11.0.min.js"></script>

<script>$(document).ready(function(c) {
	$('.close').on('click', function(c){
		$('.cake-top').fadeOut('slow', function(c){
	  		$('.cake-top').remove();
		});
	});	  
});
</script>

<script>$(document).ready(function(c) {
	$('.close-btm').on('click', function(c){
		$('.cake-bottom').fadeOut('slow', function(c){
	  		$('.cake-bottom').remove();
		});
	});	  
});
</script>
</head>
<body style="background: #387296">
<div class="logo">
	<h3>Naval Services</h3>
</div>
<div class="cart">
   <div class="cart-top">
   	  <div class="cart-experience">
   	  	 <h4>Order History</h4>
   	  </div>
   	  <div class="cart-login">
   	  	 <div class="cart-login-img">
   	  	 	
   	  	 </div>
   	  	 <div class="cart-login-text">
   	  	 	<h5>Logged in as</h5>
   	  	 </div>

          <div class="cart-login-text">
            <h5><?php echo e($customer_name); ?></h5>
          </div>  	
   	  	    	 
   	  	 <div class="clear"> </div>
   	  </div>
   	 <div class="clear"> </div>
   </div>
   <div class="cart-bottom">
   	 <div class="table">
   	 	<table>
   	 		<tbody>
   	 	      <tr  class="main-heading">	  	      	
		 			<th></th>
		 			<th class="long-txt">Order Number</th>
		 			<th></th>
		 			<th></th>
		 			<th>Total</th> 
               <th>Order Details</th>		 			 	
   	 	     </tr>
   	 	     <?php foreach($orders as $key => $item): ?>
   	 	     <tr class="cake-top">
               <td class="cakes">              
                  
                 </td>
                 <td class="cake-text">
                  <div class="product-text">
                     <h3><?php echo e($item->order_no); ?></h3>
                     
                  </div>
             </td>
             <td class="quantity">            
                 
               </td>
               <td class="price">
                                 
               </td>
               <td class="top-remove">
                  <h4>₹<?php echo e($item->sub_total); ?></h4>
                  
               </td>
               <td class="quantity">
                  <a href="/orders/<?php echo e($item->cart_id); ?>">
                                       <h5>View</h5>
                                       </a>
                     
               </td>
            
              </tr>
              <?php endforeach; ?>
   	 	   </tbody>
   	 	</table>
   	 </div>
   	 
   </div>
</div>
<div class="copy-right">
			<p>2017 Naval Services. All rights reserved | Designed by  <a href="http://techontouch.com/" target="_blank">  TechOnTouch </a></p>
</div>

</body>
</html>
