<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Review;
class ReviewsController extends Controller
{
    public function saveReview(Request $request,$id)
    {
    	$value = session('customersession');
      $product_id=($request->input('rental')=='true')?'rental_products_id':'products_id';
    	$review = new Review([
    		'comment'=>$request->input('comment'),
    		'customers_id'=>$value->id,
    		$product_id =>$id
    		]);
    	$review->save();
    	return redirect()->back();
    }
}
