<?php

namespace App\Http\Controllers\front_end;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cart;
use App\Http\Requests;
use App\Cartcontent;
use App\Product;
use Mail;
use App\Order;
use App\Orderdetail;

use App\Rentalproduct;
class CartController extends Controller
{
    public function addProduct(Request $request,$pid)
    {
    	$qty=$request->input('quantity');
    	echo "product id".$pid;
            $value = session('customersession');
            echo $value->customer_name.",".$value->id.",qty:".$qty;
            $cid=$value->id;
            $cartcheck=Cart::where('customer_id',$value->id)->get()->count();
        if($cartcheck>0){
        	echo "cart exists";
        	$cart=Cart::where('customer_id',$value->id)->get()->first();
        	echo "cartID:".$cart->id;
        	$cartcont=Cartcontent::where('cart_id',$cart->id)->get();
        	$productstatus="notexist";
        	foreach ($cartcont as $key => $item) {
        		if($item->product_id==$pid){
        			$productstatus="exists";
        			$cont_id=$item->id;
        			$currqty=$item->quantity;
        			break;
        		}
        	}
        	echo "product status:".$productstatus;
          if(($request->input('rental'))=='false'){
        	$product=Product::where('id',$pid)->get(['discount_available','discounted_price','base_price'])->first();
          echo "this";
          }
          else{
          echo "not this";
          $product=Rentalproduct::where('id',$pid)->get(['discount_available','discounted_price','base_price'])->first();
          }
		    $price=preg_replace('/[,]/', '', ($product->discount_available=='yes')? $product->discounted_price:$product->base_price);
        	if($productstatus=="exists"){
        		//update qty
        		echo "update qty needed";
        		Cartcontent::where('id',$cont_id)->update(['quantity'=>(intval($currqty)+$qty)]);
        		$sub_total=(intval($qty)*floatval($price))+$cart->sub_total;
		    	echo "new subtotal".$sub_total;
		    	$cart->sub_total=$sub_total;
		    	$cart->save();
        	}else{
        		//insert new
		    	echo floatval($price);
		    	$sub_total=(intval($qty)*floatval($price))+$cart->sub_total;
		    	echo "new subtotal".$sub_total;
		    	$cart->sub_total=$sub_total;
		    	$cart->save();
        		$cartcontnew=new Cartcontent([
        		'cart_id'=>$cart->id,
    				'product_id'=>$pid,
    				'product_price'=>$price,
    				'quantity'=>$qty,
            'rental'=>(($request->input('rental')=='true')? 'true':'false')
        			]);
        		$cartcontnew->save();
        		echo "new content added";
        	}
        }else{
        	$this->createCart($cid,$pid,$qty,(($request->input('rental')=='true')? 'true':'false'));
        	echo "new cart created";
        }
        $request->session()->flash('status', 'Product added Successfully!');
        return redirect()->back();
    }

    public function createCart($cid,$pid,$qty,$rental)
    {
      if($rental!='true'){
    	$product=Product::where('id',$pid)->get(['discount_available','discounted_price','base_price'])->first();
      }else{
      $product=Rentalproduct::where('id',$pid)->get(['discount_available','discounted_price','base_price'])->first();
      }
      $price=preg_replace('/[,]/', '', ($product->discount_available=='yes')? $product->discounted_price:$product->base_price);
    	echo floatval($price);
    	$sub_total=(intval($qty)*floatval($price));
    	echo $sub_total;
    	$createcart=new Cart([
    		'customer_id'=>$cid,
    		'sub_total'=>$sub_total
    		]);
    	$createcart->save();

    	$cartcont=new Cartcontent([
    		'cart_id'=>$createcart->id,
    		'product_id'=>$pid,
    		'product_price'=>$price,
    		'quantity'=>$qty,
        'rental'=>(($rental=='true')? 'true':'false')
    		]);
    	$cartcont->save();
    }

    public function deleteCart()
    {
    	$value = session('customersession');
    	$cid=$value->id;
    	$cartcheck=Cart::where('customer_id',$cid)->get()->count();
    	if($cartcheck>0){
    		$getcart=Cart::where('customer_id',$cid)->get()->first();
    		$cartid=$getcart->id;
    		Cartcontent::where('cart_id',$cartid)->delete();
    	}
    	Cart::where('id',$cartid)->delete();
    	//echo "cart deleted";
        return redirect('/cart');
    }

    public function getCart(Request $request)
    {
    	$value = session('customersession');
    	$cart=Cart::where('customer_id',$value->id)->get()->first();
        if(count($cart)>0){
    	$cartcont=Cartcontent::where('cart_id',$cart->id)->get();
    	$products=array();
    	foreach ($cartcont as $key => $val) {
        if(($val->rental)!='true'){
    		$product=Product::where('id',$val->product_id)->get()->first();
      }else{
        $product=Rentalproduct::where('id',$val->product_id)->get()->first();
      }
        $products[]=$product;
    	}
    	   return view('front_end.cart',['customer_name'=>$value->customer_name,'cart'=>$cart,'cartcont'=>$cartcont,'products'=>$products]);
        }else{

           return view('front_end.cart',['customer_name'=>$value->customer_name]);
        }
    }

    public function deleteCartItem($pid)
    {
    	$value = session('customersession');
    	$cart=Cart::where('customer_id',$value->id)->get()->first();
    	Cartcontent::where('cart_id',$cart->id)->where('product_id',$pid)->delete();
    	$cartcont=Cartcontent::where('cart_id',$cart->id)->get();
    	$sub_total=0;
    	foreach ($cartcont as $key => $val) {
        if(($val->rental)!='true'){
    		$product=Product::where('id',$val->product_id)->get(['discount_available','discounted_price','base_price'])->first();
        }else{
        $product=Rentalproduct::where('id',$val->product_id)->get(['discount_available','discounted_price','base_price'])->first();
        }
        $price=preg_replace('/[,]/', '', ($product->discount_available=='yes')? $product->discounted_price:$product->base_price);
    		$sub_total=$sub_total+(floatval($price)*intval($val->quantity));
    	}
    	$cart->sub_total=$sub_total;
    	$cart->save();
    	return redirect('/cart');
    }

    public function updateCartItem(Request $request,$pid)
    {
    	$qty=intval($request->input('quantity'));
    	if($qty<1){
    		return redirect()->back()->withErrors(['Quantity cannot be negative.']);
    	}
    	$value = session('customersession');
    	$cart=Cart::where('customer_id',$value->id)->get()->first();
    	$cartcont=Cartcontent::where('cart_id',$cart->id)->where('product_id',$pid)->get()->first();
    	Cartcontent::where('id',$cartcont->id)->update(['quantity'=>$qty]);
    	$cartcont=Cartcontent::where('cart_id',$cart->id)->get();
    	$sub_total=0;
    	foreach ($cartcont as $key => $val) {
        if(($val->rental)!='true'){
    		$product=Product::where('id',$val->product_id)->get(['discount_available','discounted_price','base_price'])->first();
        }else{
        $product=Rentalproduct::where('id',$val->product_id)->get(['discount_available','discounted_price','base_price'])->first();
        }
        $price=preg_replace('/[,]/', '', ($product->discount_available=='yes')? $product->discounted_price:$product->base_price);
    		$sub_total=$sub_total+(floatval($price)*intval($val->quantity));
    	}
    	$cart->sub_total=$sub_total;
    	$cart->save();
    	return redirect('/cart');
    }

    public function sendInquiry(Request $request)
    {
    	$value = session('customersession');
    	$email=$value->email;
    	$customer_name=$value->customer_name;
    	//echo $email;
    	$cart=Cart::where('customer_id',$value->id)->get()->first();
    	$cartcont=Cartcontent::where('cart_id',$cart->id)->get();
    	$products=array();
    	foreach ($cartcont as $key => $val) {
        if(($val->rental)!='true'){
        $product=Product::where('id',$val->product_id)->get()->first();
      }else{
        $product=Rentalproduct::where('id',$val->product_id)->get()->first();
      }
    		$products[]=$product;
    	}
    	/*Mail::send('front_end.inquirysend', ['cart'=>$cart,'cartcont'=>$cartcont,'products'=>$products], function($message) {
    		$value = session('customersession');
	    	$email=$value->email;
	    	$customer_name=$value->customer_name;
   		 $message->to($email, $customer_name)->subject('Inquiry Sent to Naval');
		});
		if(count(Mail::failures()) > 0){
		    return redirect()->back()->withErrors('Email not sent.Error placing Enquiry');
		}*/
		$status="Enquiry Placed Successfully!";
		$order_no=$this->genOrderId(7);
		while((Order::where('order_no',$order_no)->get()->count())>0){
			$order_no=$this->genOrderId(7);
		}
    	$order=new Order([
    		'order_no'=>$order_no,
    		'cart_id'=>$cart->id,
    		'customer_id'=>$value->id,
    		'sub_total'=>$cart->sub_total
    		]);
    	$order->save();
    	foreach ($cartcont as $k => $v) {
    		$orderdetail=new Orderdetail([
    			'cart_id'=>$v->cart_id,
	    		'product_id'=>$v->product_id,
	    		'product_price'=>$v->product_price,
	    		'quantity'=>$v->quantity,
          'rental'=>($v->rental),
	    		'status'=>$status
    			]);
    		$orderdetail->save();
    	}
    	$this->deleteCart();
		return redirect('/');
    }


    public function genOrderId($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
		{
		    $str = '';
		    $count = strlen($charset);
		    while ($length--) {
		        $str .= $charset[mt_rand(0, $count-1)];
		    }

		    return $str;
		}
}
