<?php

namespace App\Http\Controllers\front_end;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Rentalproduct;
use App\Categorie;
use App\Review;
use App\Customer;
use App\Subcategorie;
use App\RentalCategorie;
use App\Rentalsubcategorie;
class RentalCategorieController extends Controller
{
  public function index($categorie,$id,$subcategorie=null,$subid=null)
  {
    if($subid==null){
    $products=Rentalproduct::where('rentalcategories_id',$id)->get();
    }else{
    $products=Rentalproduct::where('rentalcategories_id',$id)->where('rentalsubcategories_id',$subid)->get();
    }
    $featuredproducts=Rentalproduct::where('isfeatured','yes')->get();
    $categories=Categorie::all();
    $subcategories=Rentalsubcategorie::all();
    $rentalcategories=RentalCategorie::all();
    return view('front_end.products',['categories'=>$categories,'subcategories'=>$subcategories,'rentalcategories'=>$rentalcategories,'featuredproducts'=>$featuredproducts,'products'=>$products,'rental'=>true]);
  }

  public function getProduct($categorie,$id)
  {
    //echo $id;
      //$value = session('customersession');

    $product=Rentalproduct::where('id',$id)->get()->first();
    if(count($product)!=1){
      return redirect('404');
    }
      $reviews=Review::where('rental_products_id',$id)->get();
      $customers=array();
      foreach ($reviews as $key => $value) {
          $customer=Customer::where('id',$value->customers_id)->get(['customer_name'])->first();
          $customers[]=$customer;
      }
      $categories=Categorie::all();
      $subcategories=Subcategorie::all();
      $rentalcategories=RentalCategorie::all();
    return view('front_end.product',['categories'=>$categories,'subcategories'=>$subcategories,'rentalcategories'=>$rentalcategories,'product'=>$product,'reviews'=>$reviews,'customers'=>$customers,'rental'=>'true']);
    //print_r($product);
  }
}
