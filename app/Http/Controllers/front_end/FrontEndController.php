<?php

namespace App\Http\Controllers\front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Categorie;
use App\Slider;
use App\Client;
use App\Rentalcategorie;
class FrontEndController extends Controller
{
    public function index(){
        $categories=Categorie::all();
        $rentalcategories=Rentalcategorie::all();
        $sliders=Slider::where('isactive','yes')->get();
        $clients=Client::all();
        return view('front_end.index',['categories'=>$categories,'rentalcategories'=>$rentalcategories,'sliders'=>$sliders,'clients'=>$clients]);
    }
}
