<?php

namespace App\Http\Controllers\front_end;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Categorie;
use App\Http\Requests;
use App\Review;
use App\Customer;
use App\Rentalcategorie;
use App\Subcategorie;
class CategorieController extends Controller
{
    public function index($categorie,$id,$subcategorie=null,$subid=null)
    {
      if($subid==null){
    	$products=Product::where('categories_id',$id)->get();
      }else{
      $products=Product::where('categories_id',$id)->where('subcategories_id',$subid)->get();
      }
      $featuredproducts=Product::where('isfeatured','yes')->get();
      $categories=Categorie::all();
      $subcategories=Subcategorie::all();
      $rentalcategories=RentalCategorie::all();
      return view('front_end.products',['categories'=>$categories,'subcategories'=>$subcategories,'rentalcategories'=>$rentalcategories,'featuredproducts'=>$featuredproducts,'products'=>$products,'rental'=>false]);
    }

    public function getProduct($categorie,$id)
    {
    	//echo $id;
        //$value = session('customersession');
    	$product=Product::where('id',$id)->get()->first();
      if(count($product)!=1){
        return redirect('404');
      }
        $reviews=Review::where('products_id',$id)->get();
        $customers=array();
        foreach ($reviews as $key => $value) {
            $customer=Customer::where('id',$value->customers_id)->get(['customer_name'])->first();
            $customers[]=$customer;
        }
        $categories=Categorie::all();
        $subcategories=Subcategorie::all();
        $rentalcategories=RentalCategorie::all();
    	return view('front_end.product',['categories'=>$categories,'subcategories'=>$subcategories,'rentalcategories'=>$rentalcategories,'product'=>$product,'reviews'=>$reviews,'customers'=>$customers,'rental'=>'false']);
    	//print_r($product);
    }
}
