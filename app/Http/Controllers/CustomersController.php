<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Customer;
use Hash;
class CustomersController extends Controller
{
    public function postSignin(Request $request)
    {
    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6'
    		]);
    	$customer=Customer::where('email',$request->input('email'))->get();
        $customercount=$customer->count();
        //Auth::attempt(['email'=>$request->input('email'),'password'=>$request->input('password')])
        if($customercount>0){
            //echo "exists";
            $customer=Customer::where('email',$request->input('email'))->first();
             if (Hash::check($request->input('password'), $customer->password)) 
             { 
                //valid user 
                //echo "set session here";
                $request->session()->put('customersession', $customer);
                //echo "session set";
                /*if ($request->session()->has('customersession')) {
                    $value = session('customersession');
                    echo $value->customer_name;
                }*/
                return redirect('/');
             }else{
                //invalid user
                return redirect()->back()->withErrors('Invalid Email or Password')->withInput();
             }
    	   //return redirect('/');	
    	}else{
    	return redirect()->back()->withErrors('Invalid Email or Password')->withInput();
        }
    }
    public function postSignup(Request $request)
    {
    	$this->validate($request,[
    		'name'=>'required',
    		'email'=>'required|email|unique:customers',
    		'phone'=>'required|min:10|numeric',
    		'password'=>'required|min:6',
    		'cnfpassword'=>'required|min:6|same:password'
    		]);
    	$customer=new Customer([
    		'customer_name'=>$request->input('name'),
    		'email'=>$request->input('email'),
    		'phone'=>$request->input('phone'),
    		'password'=>bcrypt($request->input('password'))
    		]);
    	$customer->save();
    	return redirect('/login');
    }
    public function getLogout(Request $request)
    {
    	$request->session()->forget('customersession');
        $request->session()->flush();
    	return redirect('/');
    }
}
