<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Product;
use App\Rentalproduct;
use App\Orderdetail;
use App\Customer;
class OrdersController extends Controller
{
    public function getCustomerOrders()
    {
    	$value=session('customersession');
    	$orders=Order::where('customer_id',$value->id)->get();
    	return view('front_end.orders',['customer_name'=>$value->customer_name,'orders'=>$orders]);
    }

    public function getOrderDetails($id)
    {
      $value=session('customersession');
      $order=Order::where('cart_id',$id)->first();
    	$orderdetail=Orderdetail::where('cart_id',$id)->get();
    	$products=array();
    	foreach ($orderdetail as $key => $val) {
        if(($val->rental)!='true'){
        $product=Product::where('id',$val->product_id)->get()->first();
        }else{
        $product=Rentalproduct::where('id',$val->product_id)->get()->first();
        }

    		$products[]=$product;
    	}
    	return view('front_end.orderdetail',['customer_name'=>$value->customer_name,'order'=>$order,'orderdetail'=>$orderdetail,'products'=>$products]);
    }

    public function getOrders()
    {
    	$orders=Order::all();
      //print_r($orders);
    	$customers=array();
    	$prods=array();
    	$orderdetails=array();
    	foreach ($orders as $key => $value) {
    		$customer=Customer::where('id',$value->customer_id)->get()->first();
    		$customers[]=$customer;
    		$orderdetail=Orderdetail::where('cart_id',$value->cart_id)->get();
        //print_r($orderdetail);
        $orderdetails[]=$orderdetail;
	    	$products=array();

        foreach ($orderdetail as $key => $val) {
          if(($val->rental)!='true'){
            ///echo "this";
          $product=Product::where('id',$val->product_id)->get()->first();
          //print_r($product);
          }
          else{
          $product=Rentalproduct::where('id',$val->product_id)->get()->first();
          //echo "not this";
          //print_r($product);
          }
          //$product=Product::where('id',$val->product_id)->get()->first();
	    		$products[]=$product;
	    	}
	    	$prods[]=$products;
    	}
      //print_r($prods);
    	return view('admin.orders',['orders'=>$orders,'customers'=>$customers,'prods'=>$prods,'orderdetails'=>$orderdetails]);
    }
}
