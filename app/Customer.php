<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'customer_name','email','phone', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
