<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartcontent extends Model
{
    protected $fillable = ['cart_id','product_id','product_price','quantity','rental'];
}
