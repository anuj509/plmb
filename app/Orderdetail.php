<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = ['cart_id','product_id','product_price','quantity','rental','status'];
}
