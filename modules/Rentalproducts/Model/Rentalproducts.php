<?php

namespace Rentalproducts\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use

class Rentalproducts extends Model
{
  use SearchableTrait;
    public $table = 'rentalproducts';


	 public $searchable = [
	    'columns' => [
            'rentalproducts.rental_product_name'=>100,
    'rentalproducts.description'=>102,
    'rentalproducts.base_price'=>103,
    'rentalproducts.discount_available'=>104,
    'rentalproducts.discounted_price'=>105,
    'rentalproducts.isnew'=>106,
        ########relation_id
'rentalsubcategories.rental_subcategory_name'=>100,

'rentalcategories.rental_category_name'=>100,

        ],
        ########join
'joins' => [
'rentalsubcategories' => ['rentalproducts.rentalsubcategories_id','rentalsubcategories.id'],
                'rentalcategories' => ['rentalproducts.rentalcategories_id','rentalcategories.id'],
            ],###
	  ];


    public $fillable = [
        'rental_product_name',
        'rental_product_images',
        'description',
        'base_price',
        'discount_available',
        'discounted_price',
        'isnew',
        'isfeatured'
    ];



    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'rental_product_name' => 'required',
        'base_price' => 'required',
        'discount_available' => 'required',
        'isnew' => 'required'
    ];
 #####relation#####
public function Rentalsubcategories()
{ return $this->belongsTo("Rentalproducts\Model\Rentalsubcategories");}
public function Rentalcategories()
{ return $this->belongsTo("Rentalproducts\Model\Rentalcategories");}
}
