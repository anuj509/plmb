<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Rentalproducts extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('rentalproducts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rental_product_name', 100);
            ####, 30);
            $table->text('description');
            $table->double('base_price');
            $table->enum('discount_available', ['yes', 'no']);
            $table->double('discounted_price');
            $table->enum('isnew', ['yes', 'no']);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Rentalproducts","description" =>"rental products module","link_name" => "rentalproducts","status"=>1,"created_at"=>"2017-10-02 08:03:11")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Rentalproducts','display_name' => 'view_Rentalproducts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Rentalproducts','display_name' => 'add_Rentalproducts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Rentalproducts','display_name' => 'edit_Rentalproducts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Rentalproducts','display_name' => 'delete_Rentalproducts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Rentalproducts')->delete();
        DB::table('permissions')->where('name',  'add_Rentalproducts')->delete();
        DB::table('permissions')->where('name',  'edit_Rentalproducts')->delete();
        DB::table('permissions')->where('name',  'delete_Rentalproducts')->delete();
        ######remove primary key
        Schema::drop('rentalproducts');
     #####end_down_function#####
    }
}
