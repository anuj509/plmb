<?php

namespace Rentalproducts\Controller;


use Rentalproducts\Model\Rentalproducts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
use Rentalsubcategories\Model\Rentalsubcategories;

use Rentalcategories\Model\Rentalcategories;

class RentalproductsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;

    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Rentalproducts = Rentalproducts::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Rentalproducts = Rentalproducts::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Rentalproducts as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Rentalproducts.pdf');
        }else { // export excel & csv
            Excel::create('Rentalproducts', function ($excel) use ($Rentalproducts) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Rentalproducts) {
                    $sheet->fromArray($Rentalproducts);
                });
            })->download($request['export_type']);
        }
    }


    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Rentalproducts = Rentalproducts::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Rentalproducts->count();
            $Rentalproducts = $Rentalproducts->slice($page * $limit, $limit);
            $Rentalproducts = new \Illuminate\Pagination\LengthAwarePaginator($Rentalproducts, $total, $limit);
        } else {  ###other
            $Rentalproducts = Rentalproducts::paginate($limit);
        }
        return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'request' => $request, 'tab' =>1, 'Rentalproducts' => $Rentalproducts]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
        if(Auth::user()->can("view_Rentalproducts")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Rentalproducts = Rentalproducts::paginate(10);
            return view("RentalproductsView::Rentalproducts")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'module_menus'=>$module_menus,'tab' => 1, 'Rentalproducts' => $Rentalproducts]);
        }else{
            return redirect('404');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
		$Rentalproducts=new Rentalproducts;
        if(Auth::user()->can("add_Rentalproducts")){  #####check permission
			if($Rentalproducts->rules){
				$validator = Validator::make($request->all(), $Rentalproducts->rules);
				if ($validator->fails()) {
					$backRentalproducts = Rentalproducts::paginate(10);
					return view("RentalproductsView::Rentalproductsajax")->withErrors($validator)->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'Rentalproducts'=>$backRentalproducts,'tab'=>2,'editRentalproducts'=>$request->all()]);
				}
			}
        if($request->input('isfeatured')=='yes'){
        $allproducts=Rentalproducts::where('isfeatured','yes')->get();
        $productcount=count($allproducts);
        if($productcount>2){
          for ($i=0;$i<($productcount-2);$i++) {
            $allproducts[$i]->isfeatured='no';
            $allproducts[$i]->save();
          }
        }
        }
	 	    $Rentalproducts=Rentalproducts::create($request->all());
	 	     Activity::log([
                'contentId'   => $Rentalproducts->id,
                'contentType' => 'Rentalproducts',
                'action'      => 'Create',
                'description' => 'Created a Rentalproducts',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	                ####Multi upload image
if (isset($request["rental_product_images"][0])&&$request["rental_product_images"][0]) {
                $rental_product_images=(json_decode($request["rental_product_images"]));
                    foreach ($rental_product_images as $val) {
					   if (file_exists(public_path()."/temp/" . $val) && $val !="")
                            File::move(public_path()."/temp/" . $val, public_path()."/uploads/" . $val);
                        ModuleAlbum::create(["name" => $val,"Module_type_id"=>$Module_type_id, "Module_id" => $Rentalproducts->id]);
                       }
                   }

            ######store
            $addRentalsubcategories = Rentalsubcategories::find($request["rentalsubcategories_id"]);
$addRentalsubcategories->Rentalproducts()->save($Rentalproducts);
            $addRentalcategories = Rentalcategories::find($request["rentalcategories_id"]);
$addRentalcategories->Rentalproducts()->save($Rentalproducts);
            $Rentalproducts = Rentalproducts::paginate(10);
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab'=>1,'flag'=>3,'Rentalproducts'=>$Rentalproducts]);
        }else{
            $Rentalproducts = Rentalproducts::paginate(10);
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab'=>1,'flag'=>6,'Rentalproducts'=>$Rentalproducts]);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
        #####edit many to many
        $Rentalproducts = Rentalproducts::paginate(10);
        $editRentalproducts = Rentalproducts::find($id);
        ####Edit multiple upload image
$editRentalproducts["rental_product_images"]=ModuleAlbum::select("name")->where("Module_id",$id)->where("Module_type_id",$Module_type_id)->lists("name")->tojson();
        return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'editRentalproducts'=>$editRentalproducts,'tab'=>2,'Rentalproducts'=>$Rentalproducts]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
		$Rentalproducts=new Rentalproducts;
        if(Auth::user()->can('edit_Rentalproducts')) {  #####check permission
			if($Rentalproducts->rules){
				$validator = Validator::make($request->all(),$Rentalproducts->rules);
				if ($validator->fails()) {
					$backRentalproducts = Rentalproducts::paginate(10);
					return view("RentalproductsView::Rentalproductsajax")->withErrors($validator)->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab' => 2,'Rentalproducts'=>$backRentalproducts, 'editRentalproducts' => $request->all()]);
				}
			}
           $Rentalproducts = Rentalproducts::find($request['id']);
           if($request->input('isfeatured')=='yes'){
           $allproducts=Rentalproducts::where('isfeatured','yes')->get();
           $productcount=count($allproducts);
           if($productcount>2){
             for ($i=0;$i<($productcount-2);$i++) {
               $allproducts[$i]->isfeatured='no';
               $allproducts[$i]->save();
             }
           }
           }
            $Rentalproducts->update($request->all());
            Activity::log([
                'contentId'   => $Rentalproducts->id,
                'contentType' => 'Rentalproducts',
                'description' => 'Update a Rentalproducts',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Rentalproducts->id,
            ]);
			            ####update Multi upload image
if ($request["rental_product_images"][0]) {
                             ModuleAlbum::where("Module_id",$request["id"])->where("Module_type_id",$Module_type_id)->delete();
                             $rental_product_images=(json_decode($request["rental_product_images"]));
                             foreach ($rental_product_images as $val) {
					             if (file_exists(public_path()."/temp/" . $val) && $val !="")
                                  File::move(public_path()."/temp/" . $val, public_path()."/uploads/" . $val);
                                  ModuleAlbum::create(["name" => $val,"Module_type_id"=>$Module_type_id, "Module_id" => $Rentalproducts->id]);
                              }
                        }

			####Upload image
			######update Many to many
            ######store
            $addRentalsubcategories = Rentalsubcategories::find($request["rentalsubcategories_id"]);
$addRentalsubcategories->Rentalproducts()->save($Rentalproducts);
            $addRentalcategories = Rentalcategories::find($request["rentalcategories_id"]);
$addRentalcategories->Rentalproducts()->save($Rentalproducts);
            $backRentalproducts = Rentalproducts::paginate(10);
            $backRentalproducts->setPath('/Rentalproducts');
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab' => 1, 'Rentalproducts' => $backRentalproducts, 'flag' => 4]);
        }else{
            $backRentalproducts = Rentalproducts::paginate(10);
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab' => 1, 'Rentalproducts' => $backRentalproducts, 'flag' => 6]);
        }
    }
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
		$Rentalsubcategories=Rentalsubcategories::all()->lists("rental_subcategory_name","id")->toarray();
		$Rentalcategories=Rentalcategories::all()->lists("rental_category_name","id")->toarray();
        if(Auth::user()->can('delete_Rentalproducts')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
ModuleAlbum::where("Module_id",$val)->where("Module_type_id",$Module_type_id)->delete();


                    $user = Rentalproducts::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Rentalproducts',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Rentalproducts',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Rentalproducts = Rentalproducts::paginate(10);
            $Rentalproducts->setPath('/Rentalproducts');
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab' => 1, 'Rentalproducts' => $Rentalproducts, 'flag' => 5]);
        }else{
            $Rentalproducts = Rentalproducts::paginate(10);
            return view("RentalproductsView::Rentalproductsajax")->with(['Rentalsubcategories'=>$Rentalsubcategories,'Rentalcategories'=>$Rentalcategories,'tab' => 1, 'Rentalproducts' => $Rentalproducts, 'flag' => 6]);
        }
    }
}
