<?php

namespace Rentalproducts;

use Illuminate\Support\ServiceProvider;

class RentalproductsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Rentalproducts',function($app){
            return new Rentalproducts;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','RentalproductsView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','RentalproductsLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}