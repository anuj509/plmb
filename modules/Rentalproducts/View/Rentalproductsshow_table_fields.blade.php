<td data-title="Category_name">@foreach($Rentalcategories as $key=>$val)
@if($content->rentalcategories_id==$key)
{{ $val }}
@endif
@endforeach
</td>
<td data-title="SubCategory_name">@foreach($Rentalsubcategories as $key=>$val)
@if($content->rentalsubcategories_id==$key)
{{ $val }}
@endif
@endforeach
</td>

<td data-title="rental_product_name">{{$content->rental_product_name}}</td>

<td data-title="rental_product_images"><img class="img-thumbnail" src="../uploads/{{json_decode($content->rental_product_images,true)[0]}}" /></td>

<td data-title="description">{{$content->description}}</td>

<td data-title="base_price">{{$content->base_price}}</td>

<td data-title="discount_available">{{$content->discount_available}}</td>

<td data-title="discounted_price">{{$content->discounted_price}}</td>

<td data-title="isnew">{{$content->isnew}}</td>

<td data-title="isnew">{{$content->isfeatured}}</td>
