<!-- Rental Product Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Rental Product Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editRentalproducts['rental_product_name']) ? $editRentalproducts['rental_product_name'] : ''}}" name="rental_product_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Rental Product Images
	</label>
	<div class="col-sm-10">
		<div  class="dropzone multi-uploader" id="dropzone" data-name="rental_product_images"></div>
		<input name="rental_product_images" type="hidden" value="{{isset($editRentalproducts['rental_product_images']) ? $editRentalproducts['rental_product_images'] : ''}}" style="width:100%;">
	</div>
</div>

<!-- Description Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Description
	</label>
	<div class="col-sm-9">
		<textarea  name="description" type="text" placeholder="" class="form-control">{{isset($editRentalproducts['description']) ? $editRentalproducts['description'] : ''}}</textarea>
	</div>
</div>

<!-- Base Price Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Base Price
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editRentalproducts['base_price']) ? $editRentalproducts['base_price'] : ''}}" name="base_price" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Discount Available Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        {!! Form::label('discount_available', 'Discount Available:') !!}
    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['discount_available'])&&$editRentalproducts['discount_available']=="yes")||!isset($editRentalproducts['discount_available']) ? 'checked' : ''}} id="active" value="yes" type="radio" name="discount_available"   class="">
    <label for="Discount Available"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['discount_available'])&&$editRentalproducts['discount_available']=="no")||!isset($editRentalproducts['discount_available']) ? 'checked' : ''}} id="active" value="no" type="radio" name="discount_available"   class="">
    <label for="Discount Available"> no</label>
</div>


    </div>
</div>

<!-- Discounted Price Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Discounted Price
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editRentalproducts['discounted_price']) ? $editRentalproducts['discounted_price'] : ''}}" name="discounted_price" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Isnew Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        {!! Form::label('isnew', 'Isnew:') !!}
    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['isnew'])&&$editRentalproducts['isnew']=="yes")||!isset($editRentalproducts['isnew']) ? 'checked' : ''}} id="active" value="yes" type="radio" name="isnew"   class="">
    <label for="Isnew"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['isnew'])&&$editRentalproducts['isnew']=="no")||!isset($editRentalproducts['isnew']) ? 'checked' : ''}} id="active" value="no" type="radio" name="isnew"   class="">
    <label for="Isnew"> no</label>
</div>


    </div>
</div>

<!-- Isfeatured Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-4">
        {!! Form::label('isfeatured', 'Isfeatured:') !!}
    </div>
    <div class="form-group col-sm-8">
        <div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['isfeatured'])&&$editRentalproducts['isfeatured']=="yes")||!isset($editRentalproducts['isfeatured']) ? 'checked' : ''}} id="active" value="yes" type="radio" name="isfeatured"   class="">
    <label for="Isfeatured"> yes</label>
</div>


<div class="radio radio-info radio-inline">
    <input {{(isset($editRentalproducts['isfeatured'])&&$editRentalproducts['isfeatured']=="no")||!isset($editRentalproducts['isfeatured']) ? 'checked' : ''}} id="active" value="no" type="radio" name="isfeatured"   class="">
    <label for="Isfeatured"> no</label>
</div>


    </div>
</div>
