<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Rentalproducts  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('rentalproducts','Rentalproducts\Controller\RentalproductsController');
    Route::post('rentalproducts/search','Rentalproducts\Controller\RentalproductsController@search');
	Route::post('rentalproducts/export/excel','Rentalproducts\Controller\RentalproductsController@export_excel');
####end
#####relation#####
});
 