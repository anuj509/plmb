<!-- Category Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Category Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editCategories['category_name']) ? $editCategories['category_name'] : ''}}" name="category_name" type="text" placeholder="" class="form-control">
	</div>
</div>
