<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Categories  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('categories','Categories\Controller\CategoriesController');
    Route::post('categories/search','Categories\Controller\CategoriesController@search');
	Route::post('categories/export/excel','Categories\Controller\CategoriesController@export_excel');
####end
#####relation#####
});
 