<?php

namespace Categories\Controller;


use Categories\Model\Categories;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class CategoriesController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Categories = Categories::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Categories = Categories::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Categories as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Categories.pdf');
        }else { // export excel & csv
            Excel::create('Categories', function ($excel) use ($Categories) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Categories) {
                    $sheet->fromArray($Categories);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Categories = Categories::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Categories->count();
            $Categories = $Categories->slice($page * $limit, $limit);
            $Categories = new \Illuminate\Pagination\LengthAwarePaginator($Categories, $total, $limit);
        } else {  ###other
            $Categories = Categories::paginate($limit);
        }
        return view("CategoriesView::Categoriesajax")->with(['request' => $request, 'tab' =>1, 'Categories' => $Categories]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Categories")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Categories = Categories::paginate(10);
            return view("CategoriesView::Categories")->with(['module_menus'=>$module_menus,'tab' => 1, 'Categories' => $Categories]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Categories=new Categories;
        if(Auth::user()->can("add_Categories")){  #####check permission
			if($Categories->rules){
				$validator = Validator::make($request->all(), $Categories->rules);
				if ($validator->fails()) {
					$backCategories = Categories::paginate(10);
					return view("CategoriesView::Categoriesajax")->withErrors($validator)->with(['Categories'=>$backCategories,'tab'=>2,'editCategories'=>$request->all()]);
				}
			}

	 	    $Categories=Categories::create($request->all());
	 	     Activity::log([
                'contentId'   => $Categories->id,
                'contentType' => 'Categories',
                'action'      => 'Create',
                'description' => 'Created a Categories',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Categories = Categories::paginate(10);
            return view("CategoriesView::Categoriesajax")->with(['tab'=>1,'flag'=>3,'Categories'=>$Categories]);
        }else{
            $Categories = Categories::paginate(10);
            return view("CategoriesView::Categoriesajax")->with(['tab'=>1,'flag'=>6,'Categories'=>$Categories]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Categories = Categories::paginate(10);
        $editCategories = Categories::find($id);
        ####Edit multiple upload image
        return view("CategoriesView::Categoriesajax")->with(['editCategories'=>$editCategories,'tab'=>2,'Categories'=>$Categories]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Categories=new Categories;
        if(Auth::user()->can('edit_Categories')) {  #####check permission
			if($Categories->rules){
				$validator = Validator::make($request->all(),$Categories->rules);
				if ($validator->fails()) {
					$backCategories = Categories::paginate(10);
					return view("CategoriesView::Categoriesajax")->withErrors($validator)->with(['tab' => 2,'Categories'=>$backCategories, 'editCategories' => $request->all()]);
				}
			}
           $Categories = Categories::find($request['id']);
            $Categories->update($request->all());
            Activity::log([
                'contentId'   => $Categories->id,
                'contentType' => 'Categories',
                'description' => 'Update a Categories',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Categories->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backCategories = Categories::paginate(10);
            $backCategories->setPath('/Categories');
            return view("CategoriesView::Categoriesajax")->with(['tab' => 1, 'Categories' => $backCategories, 'flag' => 4]);
        }else{
            $backCategories = Categories::paginate(10);
            return view("CategoriesView::Categoriesajax")->with(['tab' => 1, 'Categories' => $backCategories, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Categories')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Categories::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Categories',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Categories',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Categories = Categories::paginate(10);
            $Categories->setPath('/Categories');
            return view("CategoriesView::Categoriesajax")->with(['tab' => 1, 'Categories' => $Categories, 'flag' => 5]);
        }else{
            $Categories = Categories::paginate(10);
            return view("CategoriesView::Categoriesajax")->with(['tab' => 1, 'Categories' => $Categories, 'flag' => 6]);
        }
    }	
}
