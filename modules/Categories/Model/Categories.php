<?php

namespace Categories\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Categories extends Model
{
  use SearchableTrait;
    public $table = 'categories';
    
	
	 public $searchable = [
	    'columns' => [
            'categories.category_name'=>100,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'category_name'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'category_name' => 'required'
    ];
 #####relation#####
public function Products()
{ return $this->hasMany("Products\Model\Products");}
}
