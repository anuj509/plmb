<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Customers  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('customers','Customers\Controller\CustomersController');
    Route::post('customers/search','Customers\Controller\CustomersController@search');
	Route::post('customers/export/excel','Customers\Controller\CustomersController@export_excel');
####end
#####relation#####
});
 