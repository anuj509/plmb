<?php

namespace Customers\Controller;


use Customers\Model\Customers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class CustomersController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Customers = Customers::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Customers = Customers::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Customers as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Customers.pdf');
        }else { // export excel & csv
            Excel::create('Customers', function ($excel) use ($Customers) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Customers) {
                    $sheet->fromArray($Customers);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Customers = Customers::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Customers->count();
            $Customers = $Customers->slice($page * $limit, $limit);
            $Customers = new \Illuminate\Pagination\LengthAwarePaginator($Customers, $total, $limit);
        } else {  ###other
            $Customers = Customers::paginate($limit);
        }
        return view("CustomersView::Customersajax")->with(['request' => $request, 'tab' =>1, 'Customers' => $Customers]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Customers")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Customers = Customers::paginate(10);
            return view("CustomersView::Customers")->with(['module_menus'=>$module_menus,'tab' => 1, 'Customers' => $Customers]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Customers=new Customers;
        if(Auth::user()->can("add_Customers")){  #####check permission
			if($Customers->rules){
				$validator = Validator::make($request->all(), $Customers->rules);
				if ($validator->fails()) {
					$backCustomers = Customers::paginate(10);
					return view("CustomersView::Customersajax")->withErrors($validator)->with(['Customers'=>$backCustomers,'tab'=>2,'editCustomers'=>$request->all()]);
				}
			}

	 	    $Customers=Customers::create($request->all());
	 	     Activity::log([
                'contentId'   => $Customers->id,
                'contentType' => 'Customers',
                'action'      => 'Create',
                'description' => 'Created a Customers',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Customers = Customers::paginate(10);
            return view("CustomersView::Customersajax")->with(['tab'=>1,'flag'=>3,'Customers'=>$Customers]);
        }else{
            $Customers = Customers::paginate(10);
            return view("CustomersView::Customersajax")->with(['tab'=>1,'flag'=>6,'Customers'=>$Customers]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Customers = Customers::paginate(10);
        $editCustomers = Customers::find($id);
        ####Edit multiple upload image
        return view("CustomersView::Customersajax")->with(['editCustomers'=>$editCustomers,'tab'=>2,'Customers'=>$Customers]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Customers=new Customers;
        if(Auth::user()->can('edit_Customers')) {  #####check permission
			if($Customers->rules){
				$validator = Validator::make($request->all(),$Customers->rules);
				if ($validator->fails()) {
					$backCustomers = Customers::paginate(10);
					return view("CustomersView::Customersajax")->withErrors($validator)->with(['tab' => 2,'Customers'=>$backCustomers, 'editCustomers' => $request->all()]);
				}
			}
           $Customers = Customers::find($request['id']);
            $Customers->update($request->all());
            Activity::log([
                'contentId'   => $Customers->id,
                'contentType' => 'Customers',
                'description' => 'Update a Customers',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Customers->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backCustomers = Customers::paginate(10);
            $backCustomers->setPath('/Customers');
            return view("CustomersView::Customersajax")->with(['tab' => 1, 'Customers' => $backCustomers, 'flag' => 4]);
        }else{
            $backCustomers = Customers::paginate(10);
            return view("CustomersView::Customersajax")->with(['tab' => 1, 'Customers' => $backCustomers, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Customers')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Customers::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Customers',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Customers',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Customers = Customers::paginate(10);
            $Customers->setPath('/Customers');
            return view("CustomersView::Customersajax")->with(['tab' => 1, 'Customers' => $Customers, 'flag' => 5]);
        }else{
            $Customers = Customers::paginate(10);
            return view("CustomersView::Customersajax")->with(['tab' => 1, 'Customers' => $Customers, 'flag' => 6]);
        }
    }	
}
