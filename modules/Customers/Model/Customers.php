<?php

namespace Customers\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Customers extends Model
{
  use SearchableTrait;
    public $table = 'customers';
    
	
	 public $searchable = [
	    'columns' => [
            'customers.customer_name'=>100,
    'customers.email'=>101,
    'customers.phone'=>102,
    'customers.password'=>103,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'customer_name',
        'email',
        'phone',
        'password'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'customer_name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'password' => 'required'
    ];
 #####relation#####
public function Reviews()
{ return $this->hasMany("Reviews\Model\Reviews");}
}
