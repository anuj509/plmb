<!-- Subcategory Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Subcategory Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editSubcategories['subcategory_name']) ? $editSubcategories['subcategory_name'] : ''}}" name="subcategory_name" type="text" placeholder="" class="form-control">
	</div>
</div>
