<?php

namespace Subcategories\Controller;


use Subcategories\Model\Subcategories;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class SubcategoriesController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Subcategories = Subcategories::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Subcategories = Subcategories::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Subcategories as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Subcategories.pdf');
        }else { // export excel & csv
            Excel::create('Subcategories', function ($excel) use ($Subcategories) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Subcategories) {
                    $sheet->fromArray($Subcategories);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Subcategories = Subcategories::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Subcategories->count();
            $Subcategories = $Subcategories->slice($page * $limit, $limit);
            $Subcategories = new \Illuminate\Pagination\LengthAwarePaginator($Subcategories, $total, $limit);
        } else {  ###other
            $Subcategories = Subcategories::paginate($limit);
        }
        return view("SubcategoriesView::Subcategoriesajax")->with(['request' => $request, 'tab' =>1, 'Subcategories' => $Subcategories]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Subcategories")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Subcategories = Subcategories::paginate(10);
            return view("SubcategoriesView::Subcategories")->with(['module_menus'=>$module_menus,'tab' => 1, 'Subcategories' => $Subcategories]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Subcategories=new Subcategories;
        if(Auth::user()->can("add_Subcategories")){  #####check permission
			if($Subcategories->rules){
				$validator = Validator::make($request->all(), $Subcategories->rules);
				if ($validator->fails()) {
					$backSubcategories = Subcategories::paginate(10);
					return view("SubcategoriesView::Subcategoriesajax")->withErrors($validator)->with(['Subcategories'=>$backSubcategories,'tab'=>2,'editSubcategories'=>$request->all()]);
				}
			}

	 	    $Subcategories=Subcategories::create($request->all());
	 	     Activity::log([
                'contentId'   => $Subcategories->id,
                'contentType' => 'Subcategories',
                'action'      => 'Create',
                'description' => 'Created a Subcategories',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Subcategories = Subcategories::paginate(10);
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab'=>1,'flag'=>3,'Subcategories'=>$Subcategories]);
        }else{
            $Subcategories = Subcategories::paginate(10);
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab'=>1,'flag'=>6,'Subcategories'=>$Subcategories]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Subcategories = Subcategories::paginate(10);
        $editSubcategories = Subcategories::find($id);
        ####Edit multiple upload image
        return view("SubcategoriesView::Subcategoriesajax")->with(['editSubcategories'=>$editSubcategories,'tab'=>2,'Subcategories'=>$Subcategories]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Subcategories=new Subcategories;
        if(Auth::user()->can('edit_Subcategories')) {  #####check permission
			if($Subcategories->rules){
				$validator = Validator::make($request->all(),$Subcategories->rules);
				if ($validator->fails()) {
					$backSubcategories = Subcategories::paginate(10);
					return view("SubcategoriesView::Subcategoriesajax")->withErrors($validator)->with(['tab' => 2,'Subcategories'=>$backSubcategories, 'editSubcategories' => $request->all()]);
				}
			}
           $Subcategories = Subcategories::find($request['id']);
            $Subcategories->update($request->all());
            Activity::log([
                'contentId'   => $Subcategories->id,
                'contentType' => 'Subcategories',
                'description' => 'Update a Subcategories',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Subcategories->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backSubcategories = Subcategories::paginate(10);
            $backSubcategories->setPath('/Subcategories');
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab' => 1, 'Subcategories' => $backSubcategories, 'flag' => 4]);
        }else{
            $backSubcategories = Subcategories::paginate(10);
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab' => 1, 'Subcategories' => $backSubcategories, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Subcategories')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Subcategories::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Subcategories',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Subcategories',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Subcategories = Subcategories::paginate(10);
            $Subcategories->setPath('/Subcategories');
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab' => 1, 'Subcategories' => $Subcategories, 'flag' => 5]);
        }else{
            $Subcategories = Subcategories::paginate(10);
            return view("SubcategoriesView::Subcategoriesajax")->with(['tab' => 1, 'Subcategories' => $Subcategories, 'flag' => 6]);
        }
    }	
}
