<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Subcategories  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('subcategories','Subcategories\Controller\SubcategoriesController');
    Route::post('subcategories/search','Subcategories\Controller\SubcategoriesController@search');
	Route::post('subcategories/export/excel','Subcategories\Controller\SubcategoriesController@export_excel');
####end
#####relation#####
});
 