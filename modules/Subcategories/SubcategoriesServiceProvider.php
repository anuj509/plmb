<?php

namespace Subcategories;

use Illuminate\Support\ServiceProvider;

class SubcategoriesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Subcategories',function($app){
            return new Subcategories;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','SubcategoriesView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','SubcategoriesLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}