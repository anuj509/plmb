<?php

namespace Subcategories\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Subcategories extends Model
{
  use SearchableTrait;
    public $table = 'subcategories';
    
	
	 public $searchable = [
	    'columns' => [
            'subcategories.subcategory_name'=>100,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'subcategory_name'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'subcategory_name' => 'required'
    ];
 #####relation#####
public function Products()
{ return $this->hasMany("Products\Model\Products");}
}
