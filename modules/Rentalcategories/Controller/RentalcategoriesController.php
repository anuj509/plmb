<?php

namespace Rentalcategories\Controller;


use Rentalcategories\Model\Rentalcategories;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class RentalcategoriesController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Rentalcategories = Rentalcategories::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Rentalcategories = Rentalcategories::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Rentalcategories as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Rentalcategories.pdf');
        }else { // export excel & csv
            Excel::create('Rentalcategories', function ($excel) use ($Rentalcategories) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Rentalcategories) {
                    $sheet->fromArray($Rentalcategories);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Rentalcategories = Rentalcategories::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Rentalcategories->count();
            $Rentalcategories = $Rentalcategories->slice($page * $limit, $limit);
            $Rentalcategories = new \Illuminate\Pagination\LengthAwarePaginator($Rentalcategories, $total, $limit);
        } else {  ###other
            $Rentalcategories = Rentalcategories::paginate($limit);
        }
        return view("RentalcategoriesView::Rentalcategoriesajax")->with(['request' => $request, 'tab' =>1, 'Rentalcategories' => $Rentalcategories]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Rentalcategories")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Rentalcategories = Rentalcategories::paginate(10);
            return view("RentalcategoriesView::Rentalcategories")->with(['module_menus'=>$module_menus,'tab' => 1, 'Rentalcategories' => $Rentalcategories]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Rentalcategories=new Rentalcategories;
        if(Auth::user()->can("add_Rentalcategories")){  #####check permission
			if($Rentalcategories->rules){
				$validator = Validator::make($request->all(), $Rentalcategories->rules);
				if ($validator->fails()) {
					$backRentalcategories = Rentalcategories::paginate(10);
					return view("RentalcategoriesView::Rentalcategoriesajax")->withErrors($validator)->with(['Rentalcategories'=>$backRentalcategories,'tab'=>2,'editRentalcategories'=>$request->all()]);
				}
			}

	 	    $Rentalcategories=Rentalcategories::create($request->all());
	 	     Activity::log([
                'contentId'   => $Rentalcategories->id,
                'contentType' => 'Rentalcategories',
                'action'      => 'Create',
                'description' => 'Created a Rentalcategories',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Rentalcategories = Rentalcategories::paginate(10);
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab'=>1,'flag'=>3,'Rentalcategories'=>$Rentalcategories]);
        }else{
            $Rentalcategories = Rentalcategories::paginate(10);
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab'=>1,'flag'=>6,'Rentalcategories'=>$Rentalcategories]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Rentalcategories = Rentalcategories::paginate(10);
        $editRentalcategories = Rentalcategories::find($id);
        ####Edit multiple upload image
        return view("RentalcategoriesView::Rentalcategoriesajax")->with(['editRentalcategories'=>$editRentalcategories,'tab'=>2,'Rentalcategories'=>$Rentalcategories]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Rentalcategories=new Rentalcategories;
        if(Auth::user()->can('edit_Rentalcategories')) {  #####check permission
			if($Rentalcategories->rules){
				$validator = Validator::make($request->all(),$Rentalcategories->rules);
				if ($validator->fails()) {
					$backRentalcategories = Rentalcategories::paginate(10);
					return view("RentalcategoriesView::Rentalcategoriesajax")->withErrors($validator)->with(['tab' => 2,'Rentalcategories'=>$backRentalcategories, 'editRentalcategories' => $request->all()]);
				}
			}
           $Rentalcategories = Rentalcategories::find($request['id']);
            $Rentalcategories->update($request->all());
            Activity::log([
                'contentId'   => $Rentalcategories->id,
                'contentType' => 'Rentalcategories',
                'description' => 'Update a Rentalcategories',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Rentalcategories->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backRentalcategories = Rentalcategories::paginate(10);
            $backRentalcategories->setPath('/Rentalcategories');
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab' => 1, 'Rentalcategories' => $backRentalcategories, 'flag' => 4]);
        }else{
            $backRentalcategories = Rentalcategories::paginate(10);
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab' => 1, 'Rentalcategories' => $backRentalcategories, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Rentalcategories')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Rentalcategories::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Rentalcategories',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Rentalcategories',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Rentalcategories = Rentalcategories::paginate(10);
            $Rentalcategories->setPath('/Rentalcategories');
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab' => 1, 'Rentalcategories' => $Rentalcategories, 'flag' => 5]);
        }else{
            $Rentalcategories = Rentalcategories::paginate(10);
            return view("RentalcategoriesView::Rentalcategoriesajax")->with(['tab' => 1, 'Rentalcategories' => $Rentalcategories, 'flag' => 6]);
        }
    }	
}
