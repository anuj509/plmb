<?php

namespace Rentalcategories\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Rentalcategories extends Model
{
  use SearchableTrait;
    public $table = 'rentalcategories';
    
	
	 public $searchable = [
	    'columns' => [
            'rentalcategories.rental_category_name'=>100,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'rental_category_name'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'rental_category_name' => 'required'
    ];
 #####relation#####
public function Rentalproducts()
{ return $this->hasMany("Rentalproducts\Model\Rentalproducts");}
}
