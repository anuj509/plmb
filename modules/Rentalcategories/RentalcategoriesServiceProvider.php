<?php

namespace Rentalcategories;

use Illuminate\Support\ServiceProvider;

class RentalcategoriesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Rentalcategories',function($app){
            return new Rentalcategories;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','RentalcategoriesView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','RentalcategoriesLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}