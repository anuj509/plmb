<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Rentalcategories  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('rentalcategories','Rentalcategories\Controller\RentalcategoriesController');
    Route::post('rentalcategories/search','Rentalcategories\Controller\RentalcategoriesController@search');
	Route::post('rentalcategories/export/excel','Rentalcategories\Controller\RentalcategoriesController@export_excel');
####end
#####relation#####
});
 