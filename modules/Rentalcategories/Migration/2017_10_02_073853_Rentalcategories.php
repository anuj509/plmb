<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Rentalcategories extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('rentalcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rental_category_name', 100);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Rentalcategories","description" =>"rental categories","link_name" => "rentalcategories","status"=>1,"created_at"=>"2017-10-02 07:38:53")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Rentalcategories','display_name' => 'view_Rentalcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Rentalcategories','display_name' => 'add_Rentalcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Rentalcategories','display_name' => 'edit_Rentalcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Rentalcategories','display_name' => 'delete_Rentalcategories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Rentalcategories')->delete();
        DB::table('permissions')->where('name',  'add_Rentalcategories')->delete();
        DB::table('permissions')->where('name',  'edit_Rentalcategories')->delete();
        DB::table('permissions')->where('name',  'delete_Rentalcategories')->delete();
        ######remove primary key
        Schema::drop('rentalcategories');
     #####end_down_function#####
    }
}
