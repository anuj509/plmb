<!-- Rental Category Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Rental Category Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editRentalcategories['rental_category_name']) ? $editRentalcategories['rental_category_name'] : ''}}" name="rental_category_name" type="text" placeholder="" class="form-control">
	</div>
</div>
