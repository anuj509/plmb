<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Products  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('products','Products\Controller\ProductsController');
    Route::post('products/search','Products\Controller\ProductsController@search');
	Route::post('products/export/excel','Products\Controller\ProductsController@export_excel');
####end
#####relation#####
});
 