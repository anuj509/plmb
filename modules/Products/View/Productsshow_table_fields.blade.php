<td data-title="Category_name">@foreach($Categories as $key=>$val)
@if($content->categories_id==$key)
{{ $val }}
@endif
@endforeach
</td>
<td data-title="SubCategory_name">@foreach($Subcategories as $key=>$val)
@if($content->subcategories_id==$key)
{{ $val }}
@endif
@endforeach
</td>
<td data-title="product_name">{{$content->product_name}}</td>

<td data-title="product_images"><img class="img-thumbnail" src="../uploads/{{json_decode($content->product_images,true)[0]}}" /></td>

<td data-title="description">{{$content->description}}</td>

<td data-title="base_price">{{$content->base_price}}</td>

<td data-title="discount_available">{{$content->discount_available}}</td>

<td data-title="discounted_price">{{$content->discounted_price}}</td>

<td data-title="isnew">{{$content->isnew}}</td>

<td data-title="isfeatured">{{$content->isfeatured}}</td>
