<?php

namespace Products\Controller;


use Products\Model\Products;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
use Subcategories\Model\Subcategories;

use Categories\Model\Categories;





class ProductsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;

    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Products = Products::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Products = Products::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Products as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Products.pdf');
        }else { // export excel & csv
            Excel::create('Products', function ($excel) use ($Products) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Products) {
                    $sheet->fromArray($Products);
                });
            })->download($request['export_type']);
        }
    }


    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Products = Products::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Products->count();
            $Products = $Products->slice($page * $limit, $limit);
            $Products = new \Illuminate\Pagination\LengthAwarePaginator($Products, $total, $limit);
        } else {  ###other
            $Products = Products::paginate($limit);
        }
        return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'request' => $request, 'tab' =>1, 'Products' => $Products]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


        if(Auth::user()->can("view_Products")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Products = Products::paginate(10);
            return view("ProductsView::Products")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'module_menus'=>$module_menus,'tab' => 1, 'Products' => $Products]);
        }else{
            return redirect('404');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


		$Products=new Products;
        if(Auth::user()->can("add_Products")){  #####check permission
			if($Products->rules){
				$validator = Validator::make($request->all(), $Products->rules);
				if ($validator->fails()) {
					$backProducts = Products::paginate(10);
					return view("ProductsView::Productsajax")->withErrors($validator)->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'Products'=>$backProducts,'tab'=>2,'editProducts'=>$request->all()]);
				}
			}
      if($request->input('isfeatured')=='yes'){
      $allproducts=Products::where('isfeatured','yes')->get();
      $productcount=count($allproducts);
      if($productcount>2){
        for ($i=0;$i<($productcount-2);$i++) {
          $allproducts[$i]->isfeatured='no';
          $allproducts[$i]->save();
        }
      }
      }
	 	    $Products=Products::create($request->all());
	 	     Activity::log([
                'contentId'   => $Products->id,
                'contentType' => 'Products',
                'action'      => 'Create',
                'description' => 'Created a Products',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	                ####Multi upload image
if (isset($request["product_images"][0])&&$request["product_images"][0]) {
                $product_images=(json_decode($request["product_images"]));
                    foreach ($product_images as $val) {
					   if (file_exists(public_path()."/temp/" . $val) && $val !="")
                            File::move(public_path()."/temp/" . $val, public_path()."/uploads/" . $val);
                        ModuleAlbum::create(["name" => $val,"Module_type_id"=>$Module_type_id, "Module_id" => $Products->id]);
                       }
                   }

            ######store
            $addSubcategories = Subcategories::find($request["subcategories_id"]);
$addSubcategories->Products()->save($Products);
            $addCategories = Categories::find($request["categories_id"]);
$addCategories->Products()->save($Products);




            $Products = Products::paginate(10);
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab'=>1,'flag'=>3,'Products'=>$Products]);
        }else{
            $Products = Products::paginate(10);
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab'=>1,'flag'=>6,'Products'=>$Products]);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


        #####edit many to many
        $Products = Products::paginate(10);
        $editProducts = Products::find($id);
        ####Edit multiple upload image
$editProducts["product_images"]=ModuleAlbum::select("name")->where("Module_id",$id)->where("Module_type_id",$Module_type_id)->lists("name")->tojson();
        return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'editProducts'=>$editProducts,'tab'=>2,'Products'=>$Products]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


		$Products=new Products;
        if(Auth::user()->can('edit_Products')) {  #####check permission
			if($Products->rules){
				$validator = Validator::make($request->all(),$Products->rules);
				if ($validator->fails()) {
					$backProducts = Products::paginate(10);
					return view("ProductsView::Productsajax")->withErrors($validator)->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab' => 2,'Products'=>$backProducts, 'editProducts' => $request->all()]);
				}
			}
           $Products = Products::find($request['id']);
           if($request->input('isfeatured')=='yes'){
           $allproducts=Products::where('isfeatured','yes')->get();
           $productcount=count($allproducts);
           if($productcount>2){
             for ($i=0;$i<($productcount-2);$i++) {
               $allproducts[$i]->isfeatured='no';
               $allproducts[$i]->save();
             }
           }
           }
            $Products->update($request->all());
            Activity::log([
                'contentId'   => $Products->id,
                'contentType' => 'Products',
                'description' => 'Update a Products',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Products->id,
            ]);
			            ####update Multi upload image
if ($request["product_images"][0]) {
                             ModuleAlbum::where("Module_id",$request["id"])->where("Module_type_id",$Module_type_id)->delete();
                             $product_images=(json_decode($request["product_images"]));
                             foreach ($product_images as $val) {
					             if (file_exists(public_path()."/temp/" . $val) && $val !="")
                                  File::move(public_path()."/temp/" . $val, public_path()."/uploads/" . $val);
                                  ModuleAlbum::create(["name" => $val,"Module_type_id"=>$Module_type_id, "Module_id" => $Products->id]);
                              }
                        }

			####Upload image
			######update Many to many
            ######store
            $addSubcategories = Subcategories::find($request["subcategories_id"]);
$addSubcategories->Products()->save($Products);
            $addCategories = Categories::find($request["categories_id"]);
$addCategories->Products()->save($Products);




            $backProducts = Products::paginate(10);
            $backProducts->setPath('/Products');
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab' => 1, 'Products' => $backProducts, 'flag' => 4]);
        }else{
            $backProducts = Products::paginate(10);
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab' => 1, 'Products' => $backProducts, 'flag' => 6]);
        }
    }
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
		$Subcategories=Subcategories::all()->lists("subcategory_name","id")->toarray();
		$Categories=Categories::all()->lists("category_name","id")->toarray();


        if(Auth::user()->can('delete_Products')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
ModuleAlbum::where("Module_id",$val)->where("Module_type_id",$Module_type_id)->delete();


                    $user = Products::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Products',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Products',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Products = Products::paginate(10);
            $Products->setPath('/Products');
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab' => 1, 'Products' => $Products, 'flag' => 5]);
        }else{
            $Products = Products::paginate(10);
            return view("ProductsView::Productsajax")->with(['Subcategories'=>$Subcategories,'Categories'=>$Categories,'tab' => 1, 'Products' => $Products, 'flag' => 6]);
        }
    }
}
