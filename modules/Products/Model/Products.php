<?php

namespace Products\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use

class Products extends Model
{
  use SearchableTrait;
    public $table = 'products';


	 public $searchable = [
	    'columns' => [
            'products.product_name'=>100,
    'products.description'=>102,
    'products.base_price'=>103,
    'products.discount_available'=>104,
    'products.discounted_price'=>105,
    'products.isnew'=>106,
        ########relation_id
'subcategories.subcategory_name'=>100,

'categories.category_name'=>100,



        ],
        ########join
'joins' => [
'subcategories' => ['products.subcategories_id','subcategories.id'],
                'categories' => ['products.categories_id','categories.id'],
            ],###
'joins' => [
'subcategories' => ['products.subcategories_id','subcategories.id'],
            ],###
'joins' => [
'subcategories' => ['products.subcategories_id','subcategories.id'],
            ],###
	  ];


    public $fillable = [
        'product_name',
        'product_images',
        'description',
        'base_price',
        'discount_available',
        'discounted_price',
        'isnew',
        'isfeatured'
    ];



    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'product_name' => 'required',
        'base_price' => 'required',
        'discount_available' => 'required',
        'isnew' => 'required'
    ];
 #####relation#####
public function Subcategories()
{ return $this->belongsTo("Products\Model\Subcategories");}
public function Reviews()
{ return $this->hasMany("Reviews\Model\Reviews");}
public function Categories()
{ return $this->belongsTo("Products\Model\Categories");}
}
