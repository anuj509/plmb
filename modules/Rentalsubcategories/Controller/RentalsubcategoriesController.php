<?php

namespace Rentalsubcategories\Controller;


use Rentalsubcategories\Model\Rentalsubcategories;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class RentalsubcategoriesController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Rentalsubcategories = Rentalsubcategories::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Rentalsubcategories = Rentalsubcategories::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Rentalsubcategories as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Rentalsubcategories.pdf');
        }else { // export excel & csv
            Excel::create('Rentalsubcategories', function ($excel) use ($Rentalsubcategories) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Rentalsubcategories) {
                    $sheet->fromArray($Rentalsubcategories);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Rentalsubcategories = Rentalsubcategories::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Rentalsubcategories->count();
            $Rentalsubcategories = $Rentalsubcategories->slice($page * $limit, $limit);
            $Rentalsubcategories = new \Illuminate\Pagination\LengthAwarePaginator($Rentalsubcategories, $total, $limit);
        } else {  ###other
            $Rentalsubcategories = Rentalsubcategories::paginate($limit);
        }
        return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['request' => $request, 'tab' =>1, 'Rentalsubcategories' => $Rentalsubcategories]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Rentalsubcategories")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Rentalsubcategories = Rentalsubcategories::paginate(10);
            return view("RentalsubcategoriesView::Rentalsubcategories")->with(['module_menus'=>$module_menus,'tab' => 1, 'Rentalsubcategories' => $Rentalsubcategories]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Rentalsubcategories=new Rentalsubcategories;
        if(Auth::user()->can("add_Rentalsubcategories")){  #####check permission
			if($Rentalsubcategories->rules){
				$validator = Validator::make($request->all(), $Rentalsubcategories->rules);
				if ($validator->fails()) {
					$backRentalsubcategories = Rentalsubcategories::paginate(10);
					return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->withErrors($validator)->with(['Rentalsubcategories'=>$backRentalsubcategories,'tab'=>2,'editRentalsubcategories'=>$request->all()]);
				}
			}

	 	    $Rentalsubcategories=Rentalsubcategories::create($request->all());
	 	     Activity::log([
                'contentId'   => $Rentalsubcategories->id,
                'contentType' => 'Rentalsubcategories',
                'action'      => 'Create',
                'description' => 'Created a Rentalsubcategories',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Rentalsubcategories = Rentalsubcategories::paginate(10);
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab'=>1,'flag'=>3,'Rentalsubcategories'=>$Rentalsubcategories]);
        }else{
            $Rentalsubcategories = Rentalsubcategories::paginate(10);
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab'=>1,'flag'=>6,'Rentalsubcategories'=>$Rentalsubcategories]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Rentalsubcategories = Rentalsubcategories::paginate(10);
        $editRentalsubcategories = Rentalsubcategories::find($id);
        ####Edit multiple upload image
        return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['editRentalsubcategories'=>$editRentalsubcategories,'tab'=>2,'Rentalsubcategories'=>$Rentalsubcategories]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Rentalsubcategories=new Rentalsubcategories;
        if(Auth::user()->can('edit_Rentalsubcategories')) {  #####check permission
			if($Rentalsubcategories->rules){
				$validator = Validator::make($request->all(),$Rentalsubcategories->rules);
				if ($validator->fails()) {
					$backRentalsubcategories = Rentalsubcategories::paginate(10);
					return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->withErrors($validator)->with(['tab' => 2,'Rentalsubcategories'=>$backRentalsubcategories, 'editRentalsubcategories' => $request->all()]);
				}
			}
           $Rentalsubcategories = Rentalsubcategories::find($request['id']);
            $Rentalsubcategories->update($request->all());
            Activity::log([
                'contentId'   => $Rentalsubcategories->id,
                'contentType' => 'Rentalsubcategories',
                'description' => 'Update a Rentalsubcategories',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Rentalsubcategories->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backRentalsubcategories = Rentalsubcategories::paginate(10);
            $backRentalsubcategories->setPath('/Rentalsubcategories');
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab' => 1, 'Rentalsubcategories' => $backRentalsubcategories, 'flag' => 4]);
        }else{
            $backRentalsubcategories = Rentalsubcategories::paginate(10);
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab' => 1, 'Rentalsubcategories' => $backRentalsubcategories, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Rentalsubcategories')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Rentalsubcategories::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Rentalsubcategories',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Rentalsubcategories',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Rentalsubcategories = Rentalsubcategories::paginate(10);
            $Rentalsubcategories->setPath('/Rentalsubcategories');
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab' => 1, 'Rentalsubcategories' => $Rentalsubcategories, 'flag' => 5]);
        }else{
            $Rentalsubcategories = Rentalsubcategories::paginate(10);
            return view("RentalsubcategoriesView::Rentalsubcategoriesajax")->with(['tab' => 1, 'Rentalsubcategories' => $Rentalsubcategories, 'flag' => 6]);
        }
    }	
}
