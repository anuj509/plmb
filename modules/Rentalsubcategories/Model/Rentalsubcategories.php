<?php

namespace Rentalsubcategories\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Rentalsubcategories extends Model
{
  use SearchableTrait;
    public $table = 'rentalsubcategories';
    
	
	 public $searchable = [
	    'columns' => [
            'rentalsubcategories.rental_subcategory_name'=>100,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'rental_subcategory_name'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'rental_subcategory_name' => 'required'
    ];
 #####relation#####
public function Rentalproducts()
{ return $this->hasMany("Rentalproducts\Model\Rentalproducts");}
}
