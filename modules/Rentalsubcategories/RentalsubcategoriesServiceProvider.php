<?php

namespace Rentalsubcategories;

use Illuminate\Support\ServiceProvider;

class RentalsubcategoriesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Rentalsubcategories',function($app){
            return new Rentalsubcategories;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','RentalsubcategoriesView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','RentalsubcategoriesLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}