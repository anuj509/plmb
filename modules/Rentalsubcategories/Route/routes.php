<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Rentalsubcategories  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('rentalsubcategories','Rentalsubcategories\Controller\RentalsubcategoriesController');
    Route::post('rentalsubcategories/search','Rentalsubcategories\Controller\RentalsubcategoriesController@search');
	Route::post('rentalsubcategories/export/excel','Rentalsubcategories\Controller\RentalsubcategoriesController@export_excel');
####end
#####relation#####
});
 