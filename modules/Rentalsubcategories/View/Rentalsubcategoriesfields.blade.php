<!-- Rental Subcategory Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Rental Subcategory Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editRentalsubcategories['rental_subcategory_name']) ? $editRentalsubcategories['rental_subcategory_name'] : ''}}" name="rental_subcategory_name" type="text" placeholder="" class="form-control">
	</div>
</div>
