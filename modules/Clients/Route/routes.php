<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Clients  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('clients','Clients\Controller\ClientsController');
    Route::post('clients/search','Clients\Controller\ClientsController@search');
	Route::post('clients/export/excel','Clients\Controller\ClientsController@export_excel');
####end
#####relation#####
});
 