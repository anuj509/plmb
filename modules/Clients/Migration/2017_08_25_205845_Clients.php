<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Clients extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_name', 30);
            $table->string('client_image');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Clients","description" =>"Clients CRUD","link_name" => "clients","status"=>1,"created_at"=>"2017-08-25 20:58:45")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Clients','display_name' => 'view_Clients')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Clients','display_name' => 'add_Clients')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Clients','display_name' => 'edit_Clients')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Clients','display_name' => 'delete_Clients')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Clients')->delete();
        DB::table('permissions')->where('name',  'add_Clients')->delete();
        DB::table('permissions')->where('name',  'edit_Clients')->delete();
        DB::table('permissions')->where('name',  'delete_Clients')->delete();
        ######remove primary key
        Schema::drop('clients');
     #####end_down_function#####
    }
}
