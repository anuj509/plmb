<?php

namespace Clients\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Clients extends Model
{
  use SearchableTrait;
    public $table = 'clients';
    
	
	 public $searchable = [
	    'columns' => [
            'clients.client_name'=>100,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'client_name',
        'client_image'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'client_name' => 'required'
    ];
 #####relation#####
}
