<?php

namespace Clients\Controller;


use Clients\Model\Clients;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class ClientsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Clients = Clients::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Clients = Clients::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Clients as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Clients.pdf');
        }else { // export excel & csv
            Excel::create('Clients', function ($excel) use ($Clients) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Clients) {
                    $sheet->fromArray($Clients);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Clients = Clients::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Clients->count();
            $Clients = $Clients->slice($page * $limit, $limit);
            $Clients = new \Illuminate\Pagination\LengthAwarePaginator($Clients, $total, $limit);
        } else {  ###other
            $Clients = Clients::paginate($limit);
        }
        return view("ClientsView::Clientsajax")->with(['request' => $request, 'tab' =>1, 'Clients' => $Clients]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Clients")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Clients = Clients::paginate(10);
            return view("ClientsView::Clients")->with(['module_menus'=>$module_menus,'tab' => 1, 'Clients' => $Clients]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Clients=new Clients;
        if(Auth::user()->can("add_Clients")){  #####check permission
			if($Clients->rules){
				$validator = Validator::make($request->all(), $Clients->rules);
				if ($validator->fails()) {
					$backClients = Clients::paginate(10);
					return view("ClientsView::Clientsajax")->withErrors($validator)->with(['Clients'=>$backClients,'tab'=>2,'editClients'=>$request->all()]);
				}
			}

	 	    $Clients=Clients::create($request->all());
	 	     Activity::log([
                'contentId'   => $Clients->id,
                'contentType' => 'Clients',
                'action'      => 'Create',
                'description' => 'Created a Clients',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			        ####Upload image

           if (file_exists('temp/'.$request["client_image"]) && $request["client_image"] != "")
            File::move("temp/" . $request["client_image"], "uploads/" . $request["client_image"]);


	 	    ####Multi upload image
            ######store
            $Clients = Clients::paginate(10);
            return view("ClientsView::Clientsajax")->with(['tab'=>1,'flag'=>3,'Clients'=>$Clients]);
        }else{
            $Clients = Clients::paginate(10);
            return view("ClientsView::Clientsajax")->with(['tab'=>1,'flag'=>6,'Clients'=>$Clients]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Clients = Clients::paginate(10);
        $editClients = Clients::find($id);
        ####Edit multiple upload image
        return view("ClientsView::Clientsajax")->with(['editClients'=>$editClients,'tab'=>2,'Clients'=>$Clients]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Clients=new Clients;
        if(Auth::user()->can('edit_Clients')) {  #####check permission
			if($Clients->rules){
				$validator = Validator::make($request->all(),$Clients->rules);
				if ($validator->fails()) {
					$backClients = Clients::paginate(10);
					return view("ClientsView::Clientsajax")->withErrors($validator)->with(['tab' => 2,'Clients'=>$backClients, 'editClients' => $request->all()]);
				}
			}
           $Clients = Clients::find($request['id']);
            $Clients->update($request->all());
            Activity::log([
                'contentId'   => $Clients->id,
                'contentType' => 'Clients',
                'description' => 'Update a Clients',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Clients->id,
            ]);
			####update Multi upload image
			        ####Upload image

           if (file_exists('temp/'.$request["client_image"]) && $request["client_image"] != "")
            File::move("temp/" . $request["client_image"], "uploads/" . $request["client_image"]);


			######update Many to many
            ######store
            $backClients = Clients::paginate(10);
            $backClients->setPath('/Clients');
            return view("ClientsView::Clientsajax")->with(['tab' => 1, 'Clients' => $backClients, 'flag' => 4]);
        }else{
            $backClients = Clients::paginate(10);
            return view("ClientsView::Clientsajax")->with(['tab' => 1, 'Clients' => $backClients, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Clients')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Clients::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Clients',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Clients',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Clients = Clients::paginate(10);
            $Clients->setPath('/Clients');
            return view("ClientsView::Clientsajax")->with(['tab' => 1, 'Clients' => $Clients, 'flag' => 5]);
        }else{
            $Clients = Clients::paginate(10);
            return view("ClientsView::Clientsajax")->with(['tab' => 1, 'Clients' => $Clients, 'flag' => 6]);
        }
    }	
}
