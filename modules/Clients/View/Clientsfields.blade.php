<!-- Client Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Client Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editClients['client_name']) ? $editClients['client_name'] : ''}}" name="client_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Client Image
	</label>
	<div class="col-sm-10">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="client_image" ></div>
		<input name="client_image" type="hidden" value="{{ isset($editClients['client_image']) ? $editClients['client_image'] : ''}}">
	</div>
</div>
