<?php

namespace Reviews\Controller;


use Reviews\Model\Reviews;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
use Products\Model\Products;

use Customers\Model\Customers;

class ReviewsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Reviews = Reviews::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Reviews = Reviews::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Reviews as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Reviews.pdf');
        }else { // export excel & csv
            Excel::create('Reviews', function ($excel) use ($Reviews) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Reviews) {
                    $sheet->fromArray($Reviews);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Reviews = Reviews::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Reviews->count();
            $Reviews = $Reviews->slice($page * $limit, $limit);
            $Reviews = new \Illuminate\Pagination\LengthAwarePaginator($Reviews, $total, $limit);
        } else {  ###other
            $Reviews = Reviews::paginate($limit);
        }
        return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'request' => $request, 'tab' =>1, 'Reviews' => $Reviews]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
        if(Auth::user()->can("view_Reviews")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Reviews = Reviews::paginate(10);
            $custs=array();
            return view("ReviewsView::Reviews")->with(['Products'=>$Products,'Customers'=>$Customers,'module_menus'=>$module_menus,'tab' => 1, 'Reviews' => $Reviews]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
		$Reviews=new Reviews;
        if(Auth::user()->can("add_Reviews")){  #####check permission
			if($Reviews->rules){
				$validator = Validator::make($request->all(), $Reviews->rules);
				if ($validator->fails()) {
					$backReviews = Reviews::paginate(10);
					return view("ReviewsView::Reviewsajax")->withErrors($validator)->with(['Products'=>$Products,'Customers'=>$Customers,'Reviews'=>$backReviews,'tab'=>2,'editReviews'=>$request->all()]);
				}
			}

	 	    $Reviews=Reviews::create($request->all());
	 	     Activity::log([
                'contentId'   => $Reviews->id,
                'contentType' => 'Reviews',
                'action'      => 'Create',
                'description' => 'Created a Reviews',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $addProducts = Products::find($request["products_id"]);
$addProducts->Reviews()->save($Reviews);
            $addCustomers = Customers::find($request["customers_id"]);
$addCustomers->Reviews()->save($Reviews);
            $Reviews = Reviews::paginate(10);
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab'=>1,'flag'=>3,'Reviews'=>$Reviews]);
        }else{
            $Reviews = Reviews::paginate(10);
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab'=>1,'flag'=>6,'Reviews'=>$Reviews]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
        #####edit many to many
        $Reviews = Reviews::paginate(10);
        $editReviews = Reviews::find($id);
        ####Edit multiple upload image
        return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'editReviews'=>$editReviews,'tab'=>2,'Reviews'=>$Reviews]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
		$Reviews=new Reviews;
        if(Auth::user()->can('edit_Reviews')) {  #####check permission
			if($Reviews->rules){
				$validator = Validator::make($request->all(),$Reviews->rules);
				if ($validator->fails()) {
					$backReviews = Reviews::paginate(10);
					return view("ReviewsView::Reviewsajax")->withErrors($validator)->with(['Products'=>$Products,'Customers'=>$Customers,'tab' => 2,'Reviews'=>$backReviews, 'editReviews' => $request->all()]);
				}
			}
           $Reviews = Reviews::find($request['id']);
            $Reviews->update($request->all());
            Activity::log([
                'contentId'   => $Reviews->id,
                'contentType' => 'Reviews',
                'description' => 'Update a Reviews',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Reviews->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $addProducts = Products::find($request["products_id"]);
$addProducts->Reviews()->save($Reviews);
            $addCustomers = Customers::find($request["customers_id"]);
$addCustomers->Reviews()->save($Reviews);
            $backReviews = Reviews::paginate(10);
            $backReviews->setPath('/Reviews');
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab' => 1, 'Reviews' => $backReviews, 'flag' => 4]);
        }else{
            $backReviews = Reviews::paginate(10);
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab' => 1, 'Reviews' => $backReviews, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
		$Products=Products::all()->lists("product_name","id")->toarray();
		$Customers=Customers::all()->lists("customer_name","id")->toarray();
        if(Auth::user()->can('delete_Reviews')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Reviews::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Reviews',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Reviews',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Reviews = Reviews::paginate(10);
            $Reviews->setPath('/Reviews');
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab' => 1, 'Reviews' => $Reviews, 'flag' => 5]);
        }else{
            $Reviews = Reviews::paginate(10);
            return view("ReviewsView::Reviewsajax")->with(['Products'=>$Products,'Customers'=>$Customers,'tab' => 1, 'Reviews' => $Reviews, 'flag' => 6]);
        }
    }

    public function getReviews(Request $request)
        {   
            $Products=Products::all()->lists("product_name","id")->toarray();
            $Customers=Customers::all()->lists("customer_name","id")->toarray();
            $Reviews=Reviews::where('products_id',$request->input('product_id'))->get();
            $custs=array();
            foreach ($Reviews as $key => $value) {
                $cust=Customers::where('id',$value->customers_id)->get(['customer_name'])->first();
                $custs[]=$cust;
            }
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('ReviewsView::Reviews',['Products'=>$Products,'Customers'=>$Customers,'module_menus'=>$module_menus,'tab' => 1,'Reviews'=>$Reviews,'custs'=>$custs]);
        }	
}
