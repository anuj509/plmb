<?php

namespace Reviews\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Reviews extends Model
{
  use SearchableTrait;
    public $table = 'reviews';
    
	
	 public $searchable = [
	    'columns' => [
            'reviews.comment'=>100,
        ########relation_id
'products.product_name'=>100,
'products.description'=>102,
'products.base_price'=>103,
'products.discount_available'=>104,
'products.discounted_price'=>105,
'products.isnew'=>106,
'categories.category_name'=>100,

'customers.customer_name'=>100,
'customers.email'=>101,
'customers.phone'=>102,
'customers.password'=>103,

        ],
        ########join
'joins' => [
'products' => ['reviews.products_id','products.id'],
                'customers' => ['reviews.customers_id','customers.id'],
            ],###
	  ];
 

    public $fillable = [
        'comment'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'comment' => 'required'
    ];
 #####relation#####
public function Products()
{ return $this->belongsTo("Reviews\Model\Products");}
public function Customers()
{ return $this->belongsTo("Reviews\Model\Customers");}
}
