<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Reviews  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::get('reviews','Reviews\Controller\ReviewsController@index');
    Route::post('reviews/search','Reviews\Controller\ReviewsController@search');
	Route::post('reviews/export/excel','Reviews\Controller\ReviewsController@export_excel');
	Route::post('reviews','Reviews\Controller\ReviewsController@getReviews');
####end
#####relation#####
});
 