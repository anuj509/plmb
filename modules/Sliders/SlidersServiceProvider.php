<?php

namespace Sliders;

use Illuminate\Support\ServiceProvider;

class SlidersServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Sliders',function($app){
            return new Sliders;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','SlidersView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','SlidersLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}