<?php

namespace Sliders\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Sliders extends Model
{
  use SearchableTrait;
    public $table = 'sliders';
    
	
	 public $searchable = [
	    'columns' => [
            'sliders.image'=>100,
    'sliders.isactive'=>101,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'image',
        'isactive'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        
    ];
 #####relation#####
}
