<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Sliders  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('sliders','Sliders\Controller\SlidersController');
    Route::post('sliders/search','Sliders\Controller\SlidersController@search');
	Route::post('sliders/export/excel','Sliders\Controller\SlidersController@export_excel');
####end
#####relation#####
});
 