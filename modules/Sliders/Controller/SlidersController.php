<?php

namespace Sliders\Controller;


use Sliders\Model\Sliders;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class SlidersController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Sliders = Sliders::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Sliders = Sliders::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Sliders as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Sliders.pdf');
        }else { // export excel & csv
            Excel::create('Sliders', function ($excel) use ($Sliders) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Sliders) {
                    $sheet->fromArray($Sliders);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Sliders = Sliders::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Sliders->count();
            $Sliders = $Sliders->slice($page * $limit, $limit);
            $Sliders = new \Illuminate\Pagination\LengthAwarePaginator($Sliders, $total, $limit);
        } else {  ###other
            $Sliders = Sliders::paginate($limit);
        }
        return view("SlidersView::Slidersajax")->with(['request' => $request, 'tab' =>1, 'Sliders' => $Sliders]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Sliders")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Sliders = Sliders::paginate(10);
            return view("SlidersView::Sliders")->with(['module_menus'=>$module_menus,'tab' => 1, 'Sliders' => $Sliders]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Sliders=new Sliders;
        if(Auth::user()->can("add_Sliders")){  #####check permission
			if($Sliders->rules){
				$validator = Validator::make($request->all(), $Sliders->rules);
				if ($validator->fails()) {
					$backSliders = Sliders::paginate(10);
					return view("SlidersView::Slidersajax")->withErrors($validator)->with(['Sliders'=>$backSliders,'tab'=>2,'editSliders'=>$request->all()]);
				}
			}

	 	    $Sliders=Sliders::create($request->all());
	 	     Activity::log([
                'contentId'   => $Sliders->id,
                'contentType' => 'Sliders',
                'action'      => 'Create',
                'description' => 'Created a Sliders',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			        ####Upload image

           if (file_exists('temp/'.$request["image"]) && $request["image"] != "")
            File::move("temp/" . $request["image"], "uploads/" . $request["image"]);


	 	    ####Multi upload image
            ######store
            $Sliders = Sliders::paginate(10);
            return view("SlidersView::Slidersajax")->with(['tab'=>1,'flag'=>3,'Sliders'=>$Sliders]);
        }else{
            $Sliders = Sliders::paginate(10);
            return view("SlidersView::Slidersajax")->with(['tab'=>1,'flag'=>6,'Sliders'=>$Sliders]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Sliders = Sliders::paginate(10);
        $editSliders = Sliders::find($id);
        ####Edit multiple upload image
        return view("SlidersView::Slidersajax")->with(['editSliders'=>$editSliders,'tab'=>2,'Sliders'=>$Sliders]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Sliders=new Sliders;
        if(Auth::user()->can('edit_Sliders')) {  #####check permission
			if($Sliders->rules){
				$validator = Validator::make($request->all(),$Sliders->rules);
				if ($validator->fails()) {
					$backSliders = Sliders::paginate(10);
					return view("SlidersView::Slidersajax")->withErrors($validator)->with(['tab' => 2,'Sliders'=>$backSliders, 'editSliders' => $request->all()]);
				}
			}
           $Sliders = Sliders::find($request['id']);
            $Sliders->update($request->all());
            Activity::log([
                'contentId'   => $Sliders->id,
                'contentType' => 'Sliders',
                'description' => 'Update a Sliders',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Sliders->id,
            ]);
			####update Multi upload image
			        ####Upload image

           if (file_exists('temp/'.$request["image"]) && $request["image"] != "")
            File::move("temp/" . $request["image"], "uploads/" . $request["image"]);


			######update Many to many
            ######store
            $backSliders = Sliders::paginate(10);
            $backSliders->setPath('/Sliders');
            return view("SlidersView::Slidersajax")->with(['tab' => 1, 'Sliders' => $backSliders, 'flag' => 4]);
        }else{
            $backSliders = Sliders::paginate(10);
            return view("SlidersView::Slidersajax")->with(['tab' => 1, 'Sliders' => $backSliders, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Sliders')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Sliders::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Sliders',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Sliders',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Sliders = Sliders::paginate(10);
            $Sliders->setPath('/Sliders');
            return view("SlidersView::Slidersajax")->with(['tab' => 1, 'Sliders' => $Sliders, 'flag' => 5]);
        }else{
            $Sliders = Sliders::paginate(10);
            return view("SlidersView::Slidersajax")->with(['tab' => 1, 'Sliders' => $Sliders, 'flag' => 6]);
        }
    }	
}
